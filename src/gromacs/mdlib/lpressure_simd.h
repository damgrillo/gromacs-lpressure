/************************************************************************************************************
 This module implements the calculation of the local pressure for planar and spherical systems, following the
 method proposed by Goetz and Lipowsky, Lindahl and Edholm, and Nakamura et. al. (see references below). 

 The method divides the system in planar slides or spherical shells (depending on the selected geometry type)
 and calculates the normal and tangential components of the pressure tensor in each slice/shell.
   
 Author:
 Damian A. Grillo

 References:
 R. Goetz, R. Lipowsky, J. Chem. Phys. 108, 7397 (1998)
 Computer simulations of bilayer membranes: Self-assembly and interfacial tension
  
 E. Lindahl, O. Edholm, J. Chem. Phys. 113, 3882 (2000)
 Spatial and energetic-entropic decomposition of surface tension in lipid bilayers from MD simulations
    
 T. Nakamura, W. Shinoda, T. Ikeshoji, J. Chem. Phys. 135, 094106 (2011)
 Novel numerical method for calculating the pressure tensor in spherical coordinates for molecular systems
*************************************************************************************************************/

#ifndef _lpressure_simd_h_
#define _lpressure_simd_h_

#include "gromacs/simd/simd.h"

#if GMX_SIMD_HAVE_REAL

using namespace gmx;

/* SIMD local pressure contribution for pairs */
//
// NOTE: the tangential and normal components of the pressure (ptan and pnor) 
//       must be divided by the shell volume to obtain the correct pressure values.
//       This is done during the output stage (see write_localpd function in md_support.c) 
//  
// B       = current box coefficients 
// IB      = inverse box coefficients
// pbc     = use pbc
// geo     = geometry type
// nax     = normal axis (only for PLANAR geometry)
// contour = contour type (only for PLANAR geometry)
// ns      = number of slices/shells
// ws      = thickness of the slices/shells
// cX      = coordinates of the center
// iX      = coordinates of the i-particles
// jX      = coordinates of the j-particles
// jF      = forces acting on j-particles
// wcoMask = mask for particles within cutoff 
// ptan    = tangential pressure on each slice/shell
// pnor    = normal pressure on each slice/shell
void
calc_simd_pairs_LP(const SimdReal B[], const SimdReal IB[],  
                   int pbc, int  geo, int nax, int contour, SimdInt32 ns, SimdReal ws, 
                   const SimdReal cX[], const SimdReal iX[], const SimdReal jX[],
                   const SimdReal jF[], SimdBool wcoMask, real *ptan, real *pnor);

#endif

#endif

