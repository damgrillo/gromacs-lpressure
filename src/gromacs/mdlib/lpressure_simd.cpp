/************************************************************************************************************
 This module implements the calculation of the local pressure for planar and spherical systems, following the
 method proposed by Goetz and Lipowsky, Lindahl and Edholm, and Nakamura et. al. (see references below). 

 The method divides the system in planar slides or spherical shells (depending on the selected geometry type)
 and calculates the normal and tangential components of the pressure tensor in each slice/shell.
   
 Author:
 Damian A. Grillo

 References:
 R. Goetz, R. Lipowsky, J. Chem. Phys. 108, 7397 (1998)
 Computer simulations of bilayer membranes: Self-assembly and interfacial tension
  
 E. Lindahl, O. Edholm, J. Chem. Phys. 113, 3882 (2000)
 Spatial and energetic-entropic decomposition of surface tension in lipid bilayers from MD simulations
    
 T. Nakamura, W. Shinoda, T. Ikeshoji, J. Chem. Phys. 135, 094106 (2011)
 Novel numerical method for calculating the pressure tensor in spherical coordinates for molecular systems
*************************************************************************************************************/

#include <cstdio>
#include <cmath>
#include <vector>

#include "lpressure.h"
#include "lpressure_simd.h"

#include "gromacs/math/vec.h"
#include "gromacs/math/units.h"
#include "gromacs/mdlib/mdrun.h"
#include "gromacs/mdlib/nbnxn_simd.h"
#include "gromacs/simd/simd_math.h"
#include "gromacs/simd/vector_operations.h"
#include "gromacs/utility/smalloc.h"

/* Macros for cutoff values */
#define Z_CTOFF   1e-5
#define R_CTOFF   1e-5
#define SIN_CTOFF 1e-4
#define COS_CTOFF 1e-4

/************************************************************************************************************/
/*********************************************** AUX FUNCTIONS **********************************************/
/************************************************************************************************************/

#if GMX_SIMD_HAVE_REAL

/* SIMD far away particle */
static inline SimdBool gmx_simdcall
far_away(SimdReal x)
{
    SimdReal farAwayDist = SimdReal(100000);
    return (farAwayDist < abs(x));
}

/* SIMD floor function */ 
static inline SimdReal gmx_simdcall
floor(SimdReal x)
{
    SimdReal tx  = trunc(x); 
    SimdReal one = SimdReal(1.0);
    return (tx + trunc(x-tx-one));
}

/* SIMD matrix-vector multiplication */
static inline void gmx_simdcall
mvmul(const SimdReal M[], const SimdReal Xi[], SimdReal Xo[])
{
    Xo[0] = M[0]*Xi[0] + M[1]*Xi[1] + M[2]*Xi[2];
    Xo[1] = M[3]*Xi[0] + M[4]*Xi[1] + M[5]*Xi[2];
    Xo[2] = M[6]*Xi[0] + M[7]*Xi[1] + M[8]*Xi[2];
}

/* SIMD image particles nearest to a reference point under pbc */
// B       = current box coefficients 
// IB      = inverse box coefficients
// Xr      = coordinates of the reference point
// Xp      = coordinates of the particles
// Xim     = coordinates of the image particles
// wcoMask = mask for particles within cutoff 
static inline void gmx_simdcall
pbc_nearest_image_particles(const SimdReal B[], const SimdReal IB[], 
                            const SimdReal Xr[], const SimdReal Xp[],
                            SimdReal Xim[], SimdBool wcoMask)
{
    int d;
    SimdReal Half(0.5);
    SimdReal Xrf[3], Ximf[3]; 
    SimdBool farAwayMask = (far_away(Xp[0]) || far_away(Xp[1]) || far_away(Xp[2]));

    /* Convert to fractional coords */
    mvmul(IB, Xr, Xrf);
    mvmul(IB, Xp, Ximf);

    /* Nearest image, fractional coords */
    for (d = 0; d < 3; d++)
    {
        Ximf[d] = Ximf[d] - floor(Ximf[d] - Xrf[d] + Half);
    }

    /* Nearest image, cartesian coords */
    mvmul(B, Ximf, Xim);

    /* Reset far away particles */
    for (d = 0; d < 3; d++)
    {
        Xim[d] = blend(Xim[d], Xp[d], farAwayMask);
    }
   
    /* Reset particles beyond cuttof */
    for (d = 0; d < 3; d++)
    {
        Xim[d] = blend(Xp[d], Xim[d], wcoMask);
    }  
}

/* SIMD image pairs nearest to a reference point under pbc */
// B       = current box coefficients 
// IB      = inverse box coefficients
// Xr      = coordinates of the reference point
// iXp     = coordinates of the i-particles
// jXp     = coordinates of the j-particles
// iXim    = coordinates of the image i-particles
// jXim    = coordinates of the image j-particles
// wcoMask = mask for particles within cutoff 
void
pbc_nearest_image_pairs(const SimdReal B[], const SimdReal IB[], 
                        const SimdReal Xr[], const SimdReal iXp[], const SimdReal jXp[], 
                        SimdReal iXim[], SimdReal jXim[], SimdBool wcoMask)
{
    int d;
    SimdBool iMask;
    SimdReal id2im, jd2im; 
    SimdReal idXim[3], jdXim[3], Xmin[3];

    /* Calculate image particles nearest to the reference point */
    pbc_nearest_image_particles(B, IB, Xr, iXp, iXim, wcoMask);
    pbc_nearest_image_particles(B, IB, Xr, jXp, jXim, wcoMask);
    
    /* Calculate square distances */
    for (d = 0; d < 3; d++)
    {
        idXim[d] = iXim[d] - Xr[d];
        jdXim[d] = jXim[d] - Xr[d];
    }
    id2im = norm2(idXim[0], idXim[1], idXim[2]);  
    jd2im = norm2(jdXim[0], jdXim[1], jdXim[2]); 

    /* Get the image particles with the minimum distance to the reference */
    iMask = (id2im < jd2im); 
    for (d = 0; d < 3; d++)
    {
        Xmin[d] = blend(jdXim[d], idXim[d], iMask) + Xr[d]; 
    }

    /* Calculate the image particles nearest to the minimum distance point */
    pbc_nearest_image_particles(B, IB, Xmin, iXp, iXim, wcoMask);
    pbc_nearest_image_particles(B, IB, Xmin, jXp, jXim, wcoMask);
}


/************************************************************************************************************/
/********************************************** MAIN FUNCTIONS **********************************************/
/************************************************************************************************************/

/* SIMD local pressure contribution for pairs */
//
// NOTE: the tangential and normal components of the pressure (ptan and pnor) 
//       must be divided by the shell volume to obtain the correct pressure values.
//       This is done during the output stage (see write_localpd function in md_support.c) 
//  
// B       = current box coefficients 
// IB      = inverse box coefficients
// pbc     = use pbc
// geo     = geometry type
// nax     = normal axis (only for PLANAR geometry)
// contour = contour type (only for PLANAR geometry)
// ns      = number of slices/shells
// ws      = thickness of the slices/shells
// cX      = coordinates of the center
// iX      = coordinates of the i-particles
// jX      = coordinates of the j-particles
// jF      = forces acting on j-particles
// wcoMask = mask for particles within cutoff 
// ptan    = tangential pressure on each slice/shell
// pnor    = normal pressure on each slice/shell
void
calc_simd_pairs_LP(const SimdReal B[], const SimdReal IB[],  
                   int pbc, int  geo, int nax, int contour, SimdInt32 ns, SimdReal ws, 
                   const SimdReal cX[], const SimdReal iX[], const SimdReal jX[],
                   const SimdReal jF[], SimdBool wcoMask, real *ptan, real *pnor)
{  
    int d;
    SimdReal iXim[3], jXim[3];

    /* Calculate image pairs nearest to the center */
    if (pbc)
    {
        pbc_nearest_image_pairs(B, IB, cX, iX, jX, iXim, jXim, wcoMask); 
    }
    else
    {
        for (d = 0; d < 3; d++)
        {
            iXim[d] = iX[d];
            jXim[d] = jX[d];
        }
    }

    /* Calculation for planar systems */
    if (geo == LP_PLANAR)
    {
        SimdInt32 iZero(0);
        SimdInt32 iOne(1);

        SimdReal rZero(0.0);
        SimdReal rHalf(0.5);
        SimdReal rOne(1.0);

        SimdReal iN,  iT1, iT2;
        SimdReal jN,  jT1, jT2;
        SimdReal rN,  rT1, rT2;
        SimdReal fN,  fT1, fT2;
        SimdReal bN,  bT1, bT2;
        SimdReal frN, frT;
        SimdReal pN,  pT;
        SimdReal zmin, zmax, zu, zl;
        SimdReal invws, invdz, invvsl, fs;

        SimdInt32 i, imax, sl, d;
        SimdInt32 ni, nj, nmin, nmax, nint;

        int axn, ax1, ax2;
        int nsval = extract<0>(ns);

        /* Select normal and tangential axes */     
        axn  = (nax+0)%3;
        ax1  = (nax+1)%3;
        ax2  = (nax+2)%3; 

        iN   = iXim[axn];
        iT1  = iXim[ax1];
        iT2  = iXim[ax2];

        jN   = jXim[axn];
        jT1  = jXim[ax1];
        jT2  = jXim[ax2];

        fN   = jF[axn];
        fT1  = jF[ax1];
        fT2  = jF[ax2];

        bN   = B[3*axn+axn];
        bT1  = B[3*ax1+ax1];
        bT2  = B[3*ax2+ax2];

        /* r (= rij) */
        rN  = jN - iN;
        rT1 = jT1 - iT1;
        rT2 = jT2 - iT2;

        /* f*r */
        frN = fN*rN;
        frT = (fT1*rT1 + fT2*rT2) * rHalf;

        /* ni, nj, invws */    
        invws = inv(ws);
        ni    = cvttR2I(iN*invws);
        ni    = blend(ni, ni-iOne, cvtB2IB(iN < rZero));
        nj    = cvttR2I(jN*invws);
        nj    = blend(nj, nj-iOne, cvtB2IB(jN < rZero));
   
        /* Number of intersections between rij segment and the slices */
        nmin  = blend(nj, ni, ni < nj);
        nmax  = blend(ni, nj, ni < nj);
        nint  = nmax - nmin;

        /* zmin, zmax, invdz */
        zmin  = blend(jN, iN, iN < jN);
        zmax  = blend(iN, jN, iN < jN);
        invdz = rOne * maskzInv(zmax-zmin, zmin < zmax);

        /* i, imax */
        i    = iZero;
        imax = selectByMask(nint+iOne, cvtB2IB(wcoMask));

        /* Harasima contour */
        if (contour == LP_HC)
        {
            alignas(GMX_SIMD_ALIGNMENT) int  sli[GMX_SIMD_REAL_WIDTH];
            alignas(GMX_SIMD_ALIGNMENT) int  slj[GMX_SIMD_REAL_WIDTH];
            alignas(GMX_SIMD_ALIGNMENT) real pTv[GMX_SIMD_REAL_WIDTH];
            pT = selectByMask(frT, wcoMask);
            store(sli, ni);
            store(slj, nj);
            store(pTv, pT);
            for (int k = 0; k < GMX_SIMD_REAL_WIDTH; k++)
            {
                ptan[imodulo(sli[k], nsval)] += 0.5 * pTv[k];
                ptan[imodulo(slj[k], nsval)] += 0.5 * pTv[k];
            }            
        }

        /* Irving-Kirkwood contour */
        else if (contour == LP_IKC) 
        {
            alignas(GMX_SIMD_ALIGNMENT) int  slv[GMX_SIMD_REAL_WIDTH];
            alignas(GMX_SIMD_ALIGNMENT) real pTv[GMX_SIMD_REAL_WIDTH];
            alignas(GMX_SIMD_ALIGNMENT) real pNv[GMX_SIMD_REAL_WIDTH];

            while (anyTrue(i < imax))
            {
                sl = nmin+i;
                zl = blend(cvtI2R(sl)*ws, zmin, cvtIB2B(i == iZero));          
                zu = blend(cvtI2R(sl+iOne)*ws, zmax, cvtIB2B(i == nint));
                fs = blend((zu-zl)*invdz, rOne, cvtIB2B(nint == iZero));

                /* Calculate pressure components */
                pT = selectByMask(frT*fs, cvtIB2B(i < imax) && wcoMask);
                pN = selectByMask(frN*fs, cvtIB2B(i < imax) && wcoMask);

                /* Add pressure contributions on corresponding slices */ 
                store(slv, sl);
                store(pTv, pT);
                store(pNv, pN);
                for (int k = 0; k < GMX_SIMD_REAL_WIDTH; k++)
                {
                    ptan[imodulo(slv[k], nsval)] += pTv[k];
                    pnor[imodulo(slv[k], nsval)] += pNv[k]; 
                }
                
                i = i + iOne;
            }
        }
    }

    /* Calculation for spherical system */
    else if (geo == LP_SPHERICAL)
    {
        SimdInt32 iZero(0);
        SimdInt32 iOne(1);
        SimdInt32 iTwo(2);

        SimdReal rZero(0.0);
        SimdReal rHalf(0.5);
        SimdReal rOne(1.0);
        SimdReal rTwo(2.0);
        SimdReal rCTOFF(R_CTOFF);
        SimdReal sCTOFF(SIN_CTOFF);
        SimdReal cCTOFF(COS_CTOFF);

        SimdReal rX,   rY,   rZ,   r2,   r,  invr2,  invr; 
        SimdReal riX,  riY,  riZ,  ri2,  ri, invri2, invri; 
        SimdReal rjX,  rjY,  rjZ,  rj2,  rj;
        SimdReal fX,   fY,   fZ,   f2;
        SimdReal wX,   wY,   wZ,   w2,   w,   wsin,  invw;
        SimdReal fxrX, fxrY, fxrZ, fxr2, fxr, fxrsin;

        SimdReal fdr, fdrcos, fxrdw;
        SimdReal invf, invws;
        SimdReal r0, s0, lamb0;
        SimdReal ffT, ffN;
        SimdReal s, spre;
        SimdReal l, lpre;
        SimdReal pT, pN;

        SimdReal atdf, ldf, sdf;

        SimdInt32 ni, nj, n0, nmin, nsneg, nspos;
        SimdInt32 nc, ncc, ncmax;
        SimdInt32 sh, shpre, shcont;

        SimdBool  rMask,  riMask, wMask, fdrMask, fxrMask, invMask, ccMask; 
        SimdIBool spMask, nnMask, ppMask, epMask;

        alignas(GMX_SIMD_ALIGNMENT) int  shv[GMX_SIMD_REAL_WIDTH];
        alignas(GMX_SIMD_ALIGNMENT) real pTv[GMX_SIMD_REAL_WIDTH];
        alignas(GMX_SIMD_ALIGNMENT) real pNv[GMX_SIMD_REAL_WIDTH];

        /* ri */
        riX    = iXim[0] - cX[0];
        riY    = iXim[1] - cX[1];
        riZ    = iXim[2] - cX[2];
        ri2    = norm2(riX, riY, riZ);
        ri     = sqrt(ri2);
        riMask = (rCTOFF < ri);
        invri2 = maskzInv(ri2, riMask);
        invri  = maskzInvsqrt(ri2, riMask);

        /* rj */
        rjX = jXim[0] - cX[0];
        rjY = jXim[1] - cX[1];
        rjZ = jXim[2] - cX[2];
        rj2 = norm2(rjX, rjY, rjZ); 
        rj  = sqrt(rj2);

        /* r (same as rij) */
        rX    = rjX - riX;
        rY    = rjY - riY;
        rZ    = rjZ - riZ;
        r2    = norm2(rX, rY, rZ);
        r     = sqrt(r2);
        rMask = (rCTOFF < r);
        invr2 = maskzInv(r2, rMask);
        invr  = maskzInvsqrt(r2, rMask);

        /* f */
        fX = jF[0];
        fY = jF[1];
        fZ = jF[2];
        f2   = norm2(fX, fY, fZ);
        invf = maskzInvsqrt(f2, wcoMask);

        /* w (omega) */  
        cprod(rX, rY, rZ, riX, riY, riZ, &wX, &wY, &wZ);
        w2    = norm2(wX, wY, wZ);
        w     = sqrt(w2);
        wsin  = w * invr * invri;
        wMask = (sCTOFF < wsin);
        invw  = maskzInvsqrt(w2, wMask);

        /* fxr (cross product of f and r) */
        cprod(fX, fY, fZ, rX, rY, rZ, &fxrX, &fxrY, &fxrZ); 
        fxr2    = norm2(fxrX, fxrY, fxrZ);
        fxr     = sqrt(fxr2);
        fxrsin  = fxr * invf * invr;
        fxrMask = (sCTOFF < fxrsin);
        fxr     = selectByMask(fxr, fxrMask && wcoMask);

        /* fdr (dot product of f and r) */
        fdr     = iprod(fX, fY, fZ, rX, rY, rZ); 
        fdrcos  = abs(fdr) * invf * invr;
        fdrMask = (cCTOFF < fdrcos);
        fdr     = selectByMask(fdr, fdrMask && wcoMask);

        /* fxrdw (dot product of fxr and w) */
        fxrdw = iprod(fxrX, fxrY, fxrZ, wX, wY, wZ);
        fxrdw = selectByMask(fxrdw, fxrMask && wMask && wcoMask);

        /* r0, s0, lamb0 */
        r0    = w * invr;
        s0    = iprod(rX, rY, rZ, riX, riY, riZ);
        lamb0 = -s0 * invr2;

        /* ni, nj, n0 */    
        invws = inv(ws);
        ni    = cvttR2I(ri*invws);
        ni    = blend(ns, ni, (ni < ns) && cvtB2IB(wcoMask)); 
        nj    = cvttR2I(rj*invws);
        nj    = blend(ns, nj, (nj < ns) && cvtB2IB(wcoMask)); 
        n0    = cvttR2I(r0*invws);
        n0    = blend(ns, n0, (n0 < ns) && cvtB2IB(wcoMask)); 

        /* Force factors: tangential and normal components respect to r */
        ffT = fdr * invr2;
        ffN = fxrdw * invr2;

        /* Number of intersections between the line segment l connecting ri and rj and the shells */
        // nmin  = number of the min shell touched by l
        // nsneg = number of intersections corresponding to s = l*rij < 0
        // nspos = number of intersections corresponding to s = l*rij > 0 
        //
        // If lamb0 < 0, nmin = ni and there are only positive intersections (s > 0)
        // If lamb0 > 1, nmin = nj and there are only negative intersections (s < 0)
        // Else, nmin = n0 and there are both negative and positive intersections

        /* nmin, nsneg, nspos */
        nmin  = blend(n0, ni, cvtB2IB(lamb0 < rZero));
        nmin  = blend(nmin, nj, cvtB2IB(rOne < lamb0));
        nsneg = ni - nmin;
        nspos = nj - nmin;

        /* nc, ncc, ncmax */
        nc    = iZero;
        ncc   = iZero;
        ncmax = selectByMask(nsneg+nspos+iTwo, (nmin < ns) && cvtB2IB(wcoMask));

        /* Start loop */
        while (anyTrue(nc < ncmax))
        {
            pT = rZero;
            pN = rZero;

            /* Set masks for start point, negative and positive intersections, and end point */
            spMask = (nc == iZero);
            nnMask = ((iZero < nc) && (nc < nsneg+iOne));
            ppMask = ((nsneg < nc) && (nc < ncmax-iOne));
            epMask = (nc == ncmax-iOne);

            /* sh */
            sh = blend(ni, ni-nc+iOne, nnMask);
            sh = blend(sh, nmin+nc-nsneg, ppMask);
            sh = blend(sh, nj, epMask);
            
            /* l */
            l = blend(ri, ws*cvtI2R(sh), cvtIB2B(nnMask || ppMask));
            l = blend(l, rj, cvtIB2B(epMask));

            /* s */
            s = blend(s0, -sqrt(abs(r2*l*l-w2)), cvtIB2B(nnMask));
            s = blend(s, sqrt(abs(r2*l*l-w2)), cvtIB2B(ppMask));
            s = blend(s, s0+r2, cvtIB2B(epMask));

            /* Mask for invalid points */
            invMask = cvtIB2B((spMask && (ni == ns)) || (epMask && (nj == ns)));

            /* Increment count for valid points and set mask for count condition */
            ncc    = ncc + selectByNotMask(iOne, cvtB2IB(invMask));
            ccMask = cvtIB2B((iOne < ncc) && (nc < ncmax));

            /* Calculate pressure contributions */
            if (anyTrue(ccMask))
            {
                /* pt contribution from f component tangential to r */
                atdf = selectByMask(atan(s*invw), ccMask) -
                       selectByMask(atan(spre*invw), ccMask);
                pT   = pT + rHalf * ffT * w * selectByNotMask(atdf, invMask);

                /* pt contribution from f component normal to r */
                ldf = selectByMask(log(l), ccMask && (rCTOFF < l)) -
                      selectByMask(log(lpre), ccMask && (rCTOFF < lpre));
                pT  = pT + rHalf * ffN * selectByNotMask(ldf, invMask);

                /* pn contribution */
                sdf = selectByMask(s-spre, ccMask);
                pN  = ffT * selectByNotMask(sdf, invMask) - rTwo * pT;

                /* Slices/Shells where contributions will be added */
                shcont = blend(shpre, sh, sh < shpre);

                /* Add pressure contributions on corresponding shells */ 
                store(shv, shcont);
                store(pTv, pT);
                store(pNv, pN);
                for (int k = 0; k < GMX_SIMD_REAL_WIDTH; k++)
                {
                    ptan[shv[k]] += pTv[k];
                    pnor[shv[k]] += pNv[k]; 
                }
            }

            /* Save sh, l, s for next iteration and increment nc */
            shpre = sh;
            lpre  = l;
            spre  = s;
            nc    = nc + iOne;
        }
    }
}

#endif

