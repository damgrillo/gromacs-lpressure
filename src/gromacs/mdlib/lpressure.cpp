/************************************************************************************************************
 This module implements the calculation of the local pressure for planar and spherical systems, following the
 method proposed by Goetz and Lipowsky, Lindahl and Edholm, and Nakamura et. al. (see references below). 

 The method divides the system in planar slides or spherical shells (depending on the selected geometry type)
 and calculates the normal and tangential components of the pressure tensor in each slice/shell.
   
 Author:
 Damian A. Grillo

 References:
 R. Goetz, R. Lipowsky, J. Chem. Phys. 108, 7397 (1998)
 Computer simulations of bilayer membranes: Self-assembly and interfacial tension
  
 E. Lindahl, O. Edholm, J. Chem. Phys. 113, 3882 (2000)
 Spatial and energetic-entropic decomposition of surface tension in lipid bilayers from MD simulations
    
 T. Nakamura, W. Shinoda, T. Ikeshoji, J. Chem. Phys. 135, 094106 (2011)
 Novel numerical method for calculating the pressure tensor in spherical coordinates for molecular systems
*************************************************************************************************************/

#include <cstdio>
#include <cstring>
#include <cctype>
#include <cmath>

#include "lpressure.h"

#include "gromacs/math/vec.h"
#include "gromacs/math/units.h"
#include "gromacs/math/utilities.h"
#include "gromacs/mdlib/mdrun.h"
#include "gromacs/utility/real.h"
#include "gromacs/utility/smalloc.h"

/* Macros for max particles in cluster */
#define MCMAX     4

/* Macros for cutoff values */
#define R_CTOFF   1e-5
#define SIN_CTOFF 1e-4
#define COS_CTOFF 1e-4

/* Macros for min, max */
#define min(x, y) (((x) < (y)) ? (x) : (y))
#define max(x, y) (((x) > (y)) ? (x) : (y))

/************************************************************************************************************/
/*********************************************** AUX FUNCTIONS **********************************************/
/************************************************************************************************************/

/* Far away particle */
bool far_away(const rvec x)
{
    return (x[0] < -100000 || x[1] < -100000 || x[2] < -100000);
}

/* Calculate image particle nearest to xr under pbc */
// box    = current box
// invbox = inverse of the box
// xr     = reference position
// x      = particle position
// xim    = image particle position
void pbc_nearest_image_particle(const matrix box, const matrix invbox, const rvec xr, const rvec x, rvec xim)
{
    rvec xrf, xif, dxf;

    /* Far away particles are copied unchanged, else image particle is calculated */
    if (far_away(x))
    {
        copy_rvec(x, xim);
    }
    else
    {
        mvmul(invbox, xr, xrf);    
        mvmul(invbox, x,  xif);

        for (int d = 0; d < DIM; d++)
        {
            dxf[d] = rmodulo((xif[d] - xrf[d]) + 0.5, 1.0) - 0.5;
        }

        rvec_add(xrf, dxf, xif);
        mvmul(box, xif, xim);
    }
}

/* Calculate image cluster nearest to xr under pbc */
// box    = current box
// invbox = inverse of the box
// xr     = reference position
// mcl    = number of particles in the cluster
// xcl    = positions of particles in the cluster
// xclim  = positions of particles in the image cluster
void pbc_nearest_image_cluster(const matrix box, const matrix invbox, const rvec xr, int mcl, const rvec *xcl, rvec *xclim)
{
    int i;
    rvec xmin, xtemp[MCMAX];

    /* Calculate image particles nearest to xr */
    for (i = 0; i < mcl; i++)
    {
        pbc_nearest_image_particle(box, invbox, xr, xcl[i], xtemp[i]);
    }

    /* Get the image particle with the shortest distance to xr */
    copy_rvec(xtemp[0], xmin);
    for (i = 1; i < mcl; i++)
    {
        if (distance2(xtemp[i], xr) < distance2(xmin, xr))
        {
            copy_rvec(xtemp[i], xmin);
        }
    }

    /* Calculate image particles nearest to xmin */
    for (i = 0; i < mcl; i++)
    {
        pbc_nearest_image_particle(box, invbox, xmin, xcl[i], xclim[i]);
    }
}

/* Calculate pressure contribution for pair */
//
// NOTE: the tangential and normal components of the pressure (pt and pn)
//       must be divided by the slice/shell volume to obtain the correct pressure values.
//       This is done during the output stage (see write_localpd function in md_support.c) 
// 
// box     = current box
// invbox  = inverse of the box
// pbc     = use pbc
// geo     = geometry type
// nax     = normal axis (only for PLANAR geometry)
// contour = contour type (only for PLANAR geometry)
// ns      = number of slices/shells
// ws      = thickness of the slices/shells
// xc      = centering position
// xi      = position of particle i
// xj      = position of particle j
// fj      = force on particle j
// pt      = tangential pressure on each slice/shell
// pn      = normal pressure on each slice/shell
void calc_pair_LP(const matrix box, const matrix invbox, int pbc, int geo, 
                  int nax, int contour, int ns, const real ws, const rvec xc, 
                  const rvec xi, const rvec xj, const rvec fj, real *pt, real *pn)
{
    /* Calculation for planar systems */
    if (geo == LP_PLANAR)
    {
        int i, sl;
        int axn, ax1, ax2;
        int ni, nj, nmin, nmax, nint;
        real zi, zj, zmin, zmax;
        real zl, zu, invdz, fs;
        real frt, frn, ptsl, pnsl;
        rvec rij;

        const real farAway = -100000;

        /* Perform calculation only if xi and xj are no far away particles */
        if (!far_away(xi) && !far_away(xj))
        {  
            /* rij, zi, zj, ni, nj */
            rvec_sub(xj, xi, rij);
            zi = xi[nax];
            zj = xj[nax];
            ni = (int) floor(zi/ws);
            nj = (int) floor(zj/ws);
          
            /* Number of intersections between rij segment and the slices */
            nmin = min(ni, nj);
            nmax = max(ni, nj);
            nint = nmax - nmin;

            /* zmin, zmax, invdz */
            zmin  = min(zi, zj);
            zmax  = max(zi, zj);
            invdz = (zmax-zmin > 0 ? 1.0/(zmax-zmin) : 0.0);

            /* f*r: normal and tangential components */    
            frn = fj[nax]*rij[nax];
            frt = (iprod(fj, rij) - frn) * 0.5;

            /* Harasima contour */
            if (contour == LP_HC)
            {
                pt[imodulo(ni, ns)] += 0.5 * frt;
                pt[imodulo(nj, ns)] += 0.5 * frt;                
            }

            /* Irving-Kirkwood contour */
            else if (contour == LP_IKC) 
            {
                for (i = 0; i <= nint; i++)
                {
                    sl = nmin+i; 
                    zl = (i == 0 ? zmin : sl*ws);          
                    zu = (i == nint ? zmax : (sl+1)*ws);
                    fs = (nint == 0 ? 1.0 : (zu-zl)*invdz);

                    /* Add pressure contributions for current slice */
                    pt[imodulo(sl, ns)] += frt * fs;
                    pn[imodulo(sl, ns)] += frn * fs;
                }
            }
        }
    }

    /* Calculation for spherical system */
    else if (geo == LP_SPHERICAL)
    {
        int ni, nj, n0;
        int nmin, nsneg, nspos;
        int nc, ncmax, ncv, ncc;
        int sh, shpre, shc;

        rvec riv, rjv, rv, wv, fxrv;
        real ri, rj, r, r2, invri, invr, invr2;
        real w, w2, wsin, invw;
        real f, invf, invws;
        real fdr, fdrcos;
        real fxr, fxrsin;
        real fxrdw, fxrdwcos;
        real r0, s0, lamb0;
        real ftf, fnf; 
        real ptsh, pnsh; 
        real l, lpre;
        real s, spre;

        const real farAway = -100000;

        /* Perform calculation only if xi and xj are no far away particles */
        if (!far_away(xi) && !far_away(xj))
        {
            /* ri */
            rvec_sub(xi, xc, riv); 
            ri    = norm(riv); 
            invri = (ri > R_CTOFF) ? 1.0/ri : 0.0;

            /* rj */  
            rvec_sub(xj, xc, rjv);
            rj = norm(rjv);  

            /* r (same as rij) */
            rvec_sub(rjv, riv, rv);  
            r     = norm(rv);
            r2    = norm2(rv);
            invr  = (r > R_CTOFF) ? 1.0/r : 0.0;
            invr2 = invr * invr;

            /* f */
            f    = norm(fj); 
            invf = 1.0/f; 

            /* w (omega) */
            cprod(rv, riv, wv);
            w    = norm(wv);
            w2   = norm2(wv);
            wsin = w * invr * invri;
            invw = (wsin > SIN_CTOFF) ? 1.0/w : 0.0;
            
            /* fxr (cross product of f and r) */
            cprod(fj, rv, fxrv);
            fxr    = norm(fxrv);
            fxrsin = fxr * invf * invr;
            fxr    = (fxrsin > SIN_CTOFF) ? fxr : 0.0;

            /* fdr (dot product of f and r) */
            fdr    = iprod(fj, rv);
            fdrcos = fdr * invf * invr;
            fdr    = (fabs(fdrcos) > COS_CTOFF) ? fdr : 0.0;

            /* fxrdw (dot product of fxr and w) */
            fxrdw = (fxr > 0 && wsin > SIN_CTOFF) ? iprod(fxrv, wv) : 0.0; 

            /* r0, s0, lamb0 */
            r0      = w * invr;
            s0      = iprod(rv, riv);
            lamb0   = -s0 * invr2;

            /* Force factors: normal and tangential components respect to r */    
            fnf = fxrdw * invr2;
            ftf = fdr * invr2;
            
            /* invwsh, ni, nj, n0 */  
            invws = 1.0 / ws;
            ni    = (int) (ri*invws);  
            ni    = (ni < ns) ? ni : ns;
            nj    = (int) (rj*invws);
            nj    = (nj < ns) ? nj : ns;
            n0    = (int) (r0*invws);
            n0    = (n0 < ns) ? n0 : ns;

            /* Number of intersections between the line segment l connecting ri and rj and the shells */
            // nmin  = number of the min shell touched by l
            // nsneg = number of intersections corresponding to s = l*rij < 0
            // nspos = number of intersections corresponding to s = l*rij > 0 
            //
            // If lamb0 < 0, nmin = ni and there are only positive intersections (s > 0)
            // If lamb0 > 1, nmin = nj and there are only negative intersections (s < 0)
            // Else, nmin = n0 and there are both negative and positive intersections

            nmin  = (lamb0 < 0) ? ni : n0;
            nmin  = (lamb0 > 1) ? nj : nmin;
            nsneg = ni - nmin;
            nspos = nj - nmin;
            ncmax = (nmin < ns) ? nsneg+nspos+2 : 0;
            ncc   = 0;

            /* Start loop */
            for (nc = 0; nc < ncmax; nc++)
            {          
                /* sh, l and s for ri */
                if (nc == 0)
                {
                    sh  = ni;
                    l   = ri;
                    s   = s0;
                    ncv = (ni < ns) ? 1 : 0;  
                }

                /* sh, l and s for intersections with s < 0 */
                else if (nc <= nsneg)
                {
                    sh   = ni-nc+1;
                    l    = ws*sh;
                    s    = -sqrt(fabs(r2*l*l-w2));
                    ncv  = 1; 
                }

                /* sh, l and s for intersections with s > 0 */
                else if (nc < nsneg+nspos+1)
                {
                    sh   = nmin+nc-nsneg;
                    l    = ws*sh;
                    s    = sqrt(fabs(r2*l*l-w2));
                    ncv  = 1; 
                }

                /* sh, l and s for rj */
                else
                {
                    sh  = nj;
                    l   = rj;
                    s   = s0 + r2;
                    ncv = (nj < ns) ? 1 : 0;  
                }

                /* Update nc count when point is valid (ncv > 0) */
                if (ncv > 0)
                {
                    ncc += 1;
                }

                /* Calculate pressure components on current shell */
                if (ncv > 0 && ncc > 1)
                {
                    ptsh = 0;
                    pnsh = 0;

                    /* pt contribution from fj component tangential to r */
                    ptsh += 0.5 * ftf * w * (atan(s*invw) - atan(spre*invw));

                    /* pt contribution from fj component normal to r */
                    if (l > R_CTOFF)
                    {
                        ptsh += 0.5 * fnf * log(l);
                    }
                    if (lpre > R_CTOFF)
                    {
                        ptsh -= 0.5 * fnf * log(lpre);
                    }

                    /* pn contribution */
                    pnsh += ftf * (s-spre) - 2*ptsh;

                    /* Shell where contributions will be added */
                    shc = (sh < shpre) ? sh : shpre;

                    /* Add pressure contributions */
                    pt[shc] += ptsh;
                    pn[shc] += pnsh; 
                }

                /* Save sh, l, s for next iteration */
                shpre = sh;
                lpre  = l;
                spre  = s;
            }
        }
    }
}


/************************************************************************************************************/
/********************************************** MAIN FUNCTIONS **********************************************/
/************************************************************************************************************/

/* Calculate local pressure contribution of m-body clusters */
//
// NOTE: the tangential and normal components of the pressure (pt and pn)
//       must be divided by the slice/shell volume to obtain the correct pressure values.
//       This is done during the output stage (see write_localpd function in md_support.c) 
//    
// lpd = local pressure/density data structure  
// mcl = number of particles in the cluster
// xcl = particles positions
// fcl = particles forces
// pt  = tangential pressure on slices/shells
// pn  = normal pressure on slices/shells
void calc_mbody_LP(const t_localpd *lpd, int mcl, const rvec *xcl, const rvec *fcl, real *pt, real *pn)
{
    int i, j;
    rvec fdiff, xclim[MCMAX];

    /* If number of particles in the cluster (mcl) satisfies 2 <= mcl <= MCMAX, proceed with calculation */
    if (2 <= mcl && mcl <= MCMAX)
    {
        /* When pbc, calculate image cluster nearest to the center, else copy cluster */
        if (lpd->pbc)
        {
            pbc_nearest_image_cluster(lpd->box, lpd->invbox, lpd->xc, mcl, xcl, xclim);
        }
        else
        {
            for (i = 0; i < mcl; i++)
            {
                copy_rvec(xcl[i], xclim[i]);
            }
        }

        /* Calculate pressure contribution using GLD */
        if (lpd->fdecomp == LP_GLD)
        {
            for (i = 0; i < mcl; i++)
            {
                for (j = i+1; j < mcl; j++)
                {
                    rvec_sub(fcl[j], fcl[i], fdiff);
                    svmul(1.0/mcl, fdiff, fdiff);
                    calc_pair_LP(lpd->box, lpd->invbox, lpd->pbc, lpd->geo, lpd->nax,
                                 lpd->contour, lpd->ns, lpd->ws, lpd->xc, 
                                 xclim[i], xclim[j], fdiff, pt, pn);  
                }
            }
        }

        /* Calculate pressure contribution using one reference particle (we choose iref = 1) */
        else if (lpd->fdecomp == LP_REF)
        {
            int iref = 1;
            for (j = 0; j < mcl; j++)
            {
                if (j != iref)
                {
                    calc_pair_LP(lpd->box, lpd->invbox, lpd->pbc, lpd->geo, lpd->nax,
                                 lpd->contour, lpd->ns, lpd->ws, lpd->xc, 
                                 xclim[iref], xclim[j], fcl[j], pt, pn);  
                }
            }
        }
    }
}

/* Calculate local pressure contribution of velocities */
//
// NOTE: the tangential and normal components of the pressure (pt and pn)
//       must be divided by the slice/shell volume to obtain the correct pressure values.
//       This is done during the output stage (see write_localpd function in md_support.c) 
//
// lpd = local pressure/density data structure  
// m   = particle mass
// x   = particle position
// v   = particle velocity
// pt  = tangential pressure on slices/shells
// pn  = normal pressure on slices/shells
void calc_veloc_LP(const t_localpd *lpd, const real m, const rvec x, const rvec v, real *pt, real *pn)
{
    int ni, sl, sh;
    real xn, v2t, v2n;
    rvec xi, ri, vnv, vtv;

    /* When pbc, calculate image position nearest to the center */
    if (lpd->pbc)
    {
        pbc_nearest_image_particle(lpd->box, lpd->invbox, lpd->xc, x, xi);
    }
    else
    {
        copy_rvec(x, xi);
    }  

    /* Calculation for planar systems */
    if (lpd->geo == LP_PLANAR)
    {
        xn  = xi[lpd->nax];
        v2n = v[lpd->nax]*v[lpd->nax];
        v2t = norm2(v) - v2n;
        sl = (int) floor(xn/lpd->ws);
        sl = imodulo(sl, lpd->ns);
        pt[sl] += 0.5 * m * v2t;
        pn[sl] += 1.0 * m * v2n;
    }

    /* Calculation for spherical systems */
    else if (lpd->geo == LP_SPHERICAL)
    {
        /* Calculate shell for image particle */
        rvec_sub(xi, lpd->xc, ri);
        sh = (int) min(norm(ri)/lpd->ws, lpd->ns);
        
        /* If sh < number of shells, proceed with calculation */
        if (sh < lpd->ns)
        {
            svmul(iprod(v, ri)/ norm2(ri), ri, vnv);
            rvec_sub(v, vnv, vtv);
            pt[sh] += 0.5 * m * norm2(vtv);
            pn[sh] += 1.0 * m * norm2(vnv);
        }
    }
}

/* Add mass contribution of a given particle to a mass group */
// lpd  = local pressure/density data structure
// m    = particle mass
// x    = particle position
// mgrp = mass of the group on slices/shells
void add_mass_LP(const t_localpd *lpd, const real m, const rvec x, real *mgrp)
{
    int sl, sh;
    rvec xi, ri;
    real xn;

    /* When pbc, calculate image position nearest to the center */
    if (lpd->pbc)
    {
        pbc_nearest_image_particle(lpd->box, lpd->invbox, lpd->xc, x, xi);
    }
    else
    {
        copy_rvec(x, xi);
    }

    /* Calculation for planar systems */
    if (lpd->geo == LP_PLANAR)
    {
        xn  = xi[lpd->nax];
        sl = (int) floor(xn/lpd->ws);
        sl = imodulo(sl, lpd->ns);
        mgrp[sl] += m;
    }

    /* Calculation for spherical systems */
    else if (lpd->geo == LP_SPHERICAL)
    {
        /* Calculate shell for image particle */
        rvec_sub(xi, lpd->xc, ri);
        sh  = (int) min(norm(ri)/lpd->ws, lpd->ns);
       
        /* If sh < number of shells, proceed with mass addition */
        if (sh < lpd->ns)
        {
            mgrp[sh] += m;
        }
    }
}


/************************************************************************************************************/
/**************************************** OTHER USEFUL FUNCTIONS ********************************************/
/************************************************************************************************************/

/* Calculate geometric center of a given group of particles under pbc */
// When pbc is used, the geometric center cannot be calculated as the usual average formula.
// We apply the method proposed by Bai and Breen (Journal of Graphics Tools, 13:4, 53-60),
// which is based on the idea of mapping each coordinate (1D) to a point onto a circle (2D).
// box    = current box
// invbox = inverse of the box
// pbc    = use pbc 
// np     = number of particles
// i      = indices of particles
// x      = positions of particles
// xc     = position of the geometric center
void calc_geometric_center(const matrix box, const matrix invbox, int pbc,
                           int np, const int *i, const rvec *x, rvec xc)
{
    int n, d;
    rvec xf, cossum, sinsum, xsum;

    clear_rvec(cossum);
    clear_rvec(sinsum);
    clear_rvec(xsum);

    for (n = 0; n < np; n++)
    {   
        /* If pbc, transform to fractional, rescale to 2PI-interval,
         * map rescaled coords to 2D points onto a circle and sum projections */
        if (pbc)
        {
            mvmul(invbox, x[i[n]], xf);
            for (d = 0; d < DIM; d++)
            {
                cossum[d] += cos(M_2PI * xf[d]);
                sinsum[d] += sin(M_2PI * xf[d]);
            }                        
        }

        /* If not pbc, sum coords directly */
        else
        {
            rvec_inc(xsum, x[i[n]]);   
        }
    }

    /* If pbc, calculate geometric center using the average projections */
    if (pbc)
    {
        for (d = 0; d < DIM; d++)
        {
            xf[d] = rmodulo(atan2(sinsum[d], cossum[d]), M_2PI) / M_2PI;
        }                      
        mvmul(box, xf, xc);
    }

    /* If not pbc, calculate geometric center with the usual average */
    else
    {
        svmul(1.0/np, xsum, xc);  
    }
}

/* Modulo function for reals */
real rmodulo(real a, real b)
{
    real res = fmod(a, b);
    if (res < 0)
        res += b;
    return res;
}

/* Modulo function for integers */
int imodulo(int a, int b)
{
    int res = a % b;
    if (res < 0)
        res += b;
    return res;
}

/* String to integer value for geometry type */
int geo_s2i_LP(const char *geo_str, char *geo_opts)
{
    int geo_int;
    strcpy(geo_opts, "planar, spherical");
    if      (strcmp(geo_str, "planar") == 0)    {geo_int = LP_PLANAR;}
    else if (strcmp(geo_str, "spherical") == 0) {geo_int = LP_SPHERICAL;}
    else                                        {geo_int = -1;}
    return geo_int;
}

/* String to integer value for normal axis */
int nax_s2i_LP(const char *naxis_str, char *naxis_opts)

{
    int naxis_int;
    strcpy(naxis_opts, "X, Y, Z");
    naxis_int   = toupper(naxis_str[0]) - 'X';
    if (strlen(naxis_str) != 1 ||  naxis_int < 0 || naxis_int > 2) {naxis_int = -1;}
    return naxis_int;
}

/* String to integer value for contribution type */
int contrib_s2i_LP(const char *contrib_str, char *contrib_opts)
{
    int contrib_int;
    strcpy(contrib_opts, "all, nb, bonds, angles, diheds, pairs, lincs, shake, veloc");
    if      (strcmp(contrib_str, "all") == 0)    {contrib_int = LP_ALL;}
    else if (strcmp(contrib_str, "nb") == 0)     {contrib_int = LP_NB;}
    else if (strcmp(contrib_str, "bonds") == 0)  {contrib_int = LP_BONDS;}
    else if (strcmp(contrib_str, "angles") == 0) {contrib_int = LP_ANGLES;}
    else if (strcmp(contrib_str, "diheds") == 0) {contrib_int = LP_DIHEDS;}
    else if (strcmp(contrib_str, "pairs") == 0)  {contrib_int = LP_PAIRS;}
    else if (strcmp(contrib_str, "lincs") == 0)  {contrib_int = LP_LINCS;}
    else if (strcmp(contrib_str, "shake") == 0)  {contrib_int = LP_SHAKE;}
    else if (strcmp(contrib_str, "veloc") == 0)  {contrib_int = LP_VELOC;}
    else                                         {contrib_int = -1;}
    return contrib_int;
}

/* String to integer value for integration contour */
int contour_s2i_LP(const char *contour_str, char *contour_opts)
{
    int contour_int;
    strcpy(contour_opts, "IK, H");
    if      (strcmp(contour_str, "IK") == 0) {contour_int =  LP_IKC;}
    else if (strcmp(contour_str, "H") == 0)  {contour_int =  LP_HC;}
    else                                     {contour_int = -1;}
    return contour_int;
}

/* String to integer value for force decomposition type */
int fdecomp_s2i_LP(const char *fdecomp_str, char *fdecomp_opts)
{
    int fdecomp_int;
    strcpy(fdecomp_opts, "REF, GLD");
    if      (strcmp(fdecomp_str, "REF") == 0) {fdecomp_int =  LP_REF;}
    else if (strcmp(fdecomp_str, "GLD") == 0) {fdecomp_int =  LP_GLD;}
    else                                      {fdecomp_int = -1;}
    return fdecomp_int;
}


