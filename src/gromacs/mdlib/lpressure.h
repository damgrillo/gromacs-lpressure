/************************************************************************************************************
 This module implements the calculation of the local pressure for planar and spherical systems, following the
 method proposed by Goetz and Lipowsky, Lindahl and Edholm, and Nakamura et. al. (see references below). 

 The method divides the system in planar slides or spherical shells (depending on the selected geometry type)
 and calculates the normal and tangential components of the pressure tensor in each slice/shell.
   
 Author:
 Damian A. Grillo

 References:
 R. Goetz, R. Lipowsky, J. Chem. Phys. 108, 7397 (1998)
 Computer simulations of bilayer membranes: Self-assembly and interfacial tension
  
 E. Lindahl, O. Edholm, J. Chem. Phys. 113, 3882 (2000)
 Spatial and energetic-entropic decomposition of surface tension in lipid bilayers from MD simulations
    
 T. Nakamura, W. Shinoda, T. Ikeshoji, J. Chem. Phys. 135, 094106 (2011)
 Novel numerical method for calculating the pressure tensor in spherical coordinates for molecular systems
*************************************************************************************************************/

#ifndef _lpressure_h_
#define _lpressure_h_

#include "gromacs/math/vec.h"

/* Macros for contribution types */
#define LP_ALL     0
#define LP_NB      1
#define LP_BONDS   2
#define LP_ANGLES  3
#define LP_DIHEDS  4
#define LP_PAIRS   5
#define LP_LINCS   6
#define LP_SHAKE   7
#define LP_VELOC   8

#define LPCALC_NB(it)     (it == LP_ALL || it == LP_NB)
#define LPCALC_BONDS(it)  (it == LP_ALL || it == LP_BONDS)
#define LPCALC_ANGLES(it) (it == LP_ALL || it == LP_ANGLES)
#define LPCALC_DIHEDS(it) (it == LP_ALL || it == LP_DIHEDS)
#define LPCALC_PAIRS(it)  (it == LP_ALL || it == LP_PAIRS)
#define LPCALC_LINCS(it)  (it == LP_ALL || it == LP_LINCS)
#define LPCALC_SHAKE(it)  (it == LP_ALL || it == LP_SHAKE)
#define LPCALC_VELOC(it)  (it == LP_ALL || it == LP_VELOC)

/* Macros for integration contours */
#define LP_IKC 0
#define LP_HC  1

/* Macros for force decomposition methods */
#define LP_REF 0
#define LP_GLD 1


/************************************************************************************************************/
/********************************************** DATA STRUCTURE **********************************************/
/************************************************************************************************************/

typedef struct
{
    int      ns;        // Number of slices/shells
    real     ws ;       // Current thickness of the slices/shells
    real     ws_sum;    // Thickness of the slices/shells summed up to the current frame
    rvec     xc;        // Centering position
    rvec     xc_sum;    // Centering position summed up to the current frame
    int      ncg;       // Number of particles in the centering group
    int  *   icg;       // Index of particles of the centering group
    int      mg;        // Number of mass groups
    char **  lmg;       // Labels of mass groups 
    int  *   nmg;       // Number of particles in mass groups
    int  **  img;       // Indices of particles in mass groups   
    real *   pn;        // Normal component of pressure tensor for current frame (size = nsh)
    real *   pn_sum;    // Normal component of pressure tensor summed up to the current frame (size = nsh)
    real *   pt;        // Tangential component of pressure tensor for current frame (size = nsh)
    real *   pt_sum;    // Tangential component of pressure tensor summed up to the current frame (size = nsh)
    real **  mass;      // Mass of each group for current frame (size = mgrps x nsh)
    real **  mass_sum;  // Mass of each group summed up to the  current frame (size = mgrps x nsh)
    int      nfr;       // Number of frames
    matrix   box;       // Current box
    matrix   invbox;    // Inverse of the box
    matrix   sumbox;    // Box summed up to the current frame
    int      geo;       // Geometry type: LP_PLANAR, LP_SPHERICAL
    int      nax;       // Normal axis: X=0, Y=1, Z=2 (only used for PLANAR geometry) 
    int      contrib;   // Contribution types for local pressure calculation 
    int      contour;   // Integration contour for local pressure calculation  (only used for PLANAR geometry) 
    int      fdecomp;   // Force decomposition method for local pressure calculation
    int      pbc;       // Use pbc
} t_localpd;


/************************************************************************************************************/
/********************************************** MAIN FUNCTIONS **********************************************/
/************************************************************************************************************/

/* Calculate local pressure contribution of m-body clusters */
// lpd = local pressure/density data structure  
// mcl = number of particles in the cluster
// xcl = particles positions
// fcl = particles forces
// pt  = tangential pressure on slices/shells
// pn  = normal pressure on slices/shells
void calc_mbody_LP(const t_localpd *lpd, int mcl, const rvec *xcl, const rvec *fcl, real *pt, real *pn);

/* Calculate local pressure contribution of velocities*/
// lpd = local pressure/density data structure  
// m   = particle mass
// x   = particle position
// v   = particle velocity
// pt  = tangential pressure on slices/shells
// pn  = normal pressure on slices/shells
void calc_veloc_LP(const t_localpd *lpd, const real m, const rvec x, const rvec v, real *pt, real *pn);

/* Add mass contribution of a given particle to a mass group */
// lpd  = local pressure/density data structure
// m    = particle mass
// x    = particle position
// mgrp = mass of the group on slices/shells
void add_mass_LP(const t_localpd *lpd, const real m, const rvec x, real *mgrp);


/************************************************************************************************************/
/**************************************** OTHER USEFUL FUNCTIONS ********************************************/
/************************************************************************************************************/

/* Calculate image particle nearest to xr under pbc */
// box    = current box
// invbox = inverse of the box
// xr     = reference position
// x      = particle position
// xim    = image particle position
void pbc_nearest_image_particle(const matrix box, const matrix invbox, const rvec xr, const rvec x, rvec xim);

/* Calculate geometric center of a given group of particles under pbc */
// When pbc is used, the geometric center cannot be calculated as the usual average formula.
// We apply the method proposed by Bai and Breen (Journal of Graphics Tools, 13:4, 53-60),
// which is based on the idea of mapping each coordinate (1D) to a point onto a circle (2D).
// box    = current box
// invbox = inverse of the box
// pbc    = use pbc 
// np     = number of particles
// i      = indices of particles
// x      = positions of particles
// xc     = position of the geometric center
void calc_geometric_center(const matrix box, const matrix invbox, int pbc,
                           int np, const int *i, const rvec *x, rvec xc);

/* Modulo function for reals */
real rmodulo(real a, real b);

/* Modulo function for integers */
int imodulo(int a, int b);

/* String to integer value for geometry type */
int geo_s2i_LP(const char *geo_str, char *geo_opts);

/* String to integer value for normal axis */
int nax_s2i_LP(const char *naxis_str, char *naxis_opts);

/* String to integer value for contribution type */
int contrib_s2i_LP(const char *contrib_str, char *contrib_opts);

/* String to integer value for integration contour */
int contour_s2i_LP(const char *contour_str, char *contour_opts);

/* String to integer value for force decomposition method */
int fdecomp_s2i_LP(const char *fdecomp_str, char *fdecomp_opts);

#endif

