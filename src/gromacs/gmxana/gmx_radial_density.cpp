/*
 * NAME
 *   g_radial_density
 *     Calculate radial density profiles for spherical systems.
 * SYNOPSIS
 *   g_radial_density -s topol.tpr -f traj.xtc -o density.xvg
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2004, The GROMACS development team.
 * Copyright (c) 2013,2014,2015,2017,2018, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
#include "gmxpre.h"

#include <cctype>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include "gromacs/commandline/pargs.h"
#include "gromacs/commandline/viewit.h"
#include "gromacs/fileio/trxio.h"
#include "gromacs/fileio/xvgr.h"
#include "gromacs/gmxana/gmx_ana.h"
#include "gromacs/gmxana/gstat.h"
#include "gromacs/gmxana/denscom.h"
#include "gromacs/math/units.h"
#include "gromacs/math/vec.h"
#include "gromacs/pbcutil/pbc.h"
#include "gromacs/pbcutil/rmpbc.h"
#include "gromacs/topology/index.h"
#include "gromacs/topology/topology.h"
#include "gromacs/utility/arraysize.h"
#include "gromacs/utility/cstringutil.h"
#include "gromacs/utility/fatalerror.h"
#include "gromacs/utility/futil.h"
#include "gromacs/utility/gmxassert.h"
#include "gromacs/utility/smalloc.h"
#include "gromacs/mdlib/lpressure.h"
#include "gromacs/mdlib/md_support.h"

static void calc_radial_electron_density(const char *fn, int **index, int gnx[],
                                         double ***shDensity, int *nshells, t_topology *top,
                                         int ePBC, int nr_grps, real *shWidth,
                                         t_electron eltab[], int nr, int *index_center, 
                                         int ncenter, gmx_bool bRelative, const gmx_output_env_t *oenv)
{
    rvec        *x0;            /* coordinates without pbc */
    rvec         xc, xd;
    matrix       box, invbox;   /* box (3x3) */
    int          natoms;        /* nr. atoms in trj */
    double       shVol;
    t_trxstatus *status;
    int          i, j, n, ax,   /* loop indices */
                 nr_frames = 0, /* number of frames */
                 shell;         /* current shell */
    t_electron  *found;         /* found by bsearch */
    t_electron   sought;        /* thingie thought by bsearch */
    gmx_rmpbc_t  gpbc = nullptr;
    real         t, rmax, avgRad, mindiff;

    /* Read coordinates */
    if ((natoms = read_first_x(oenv, &status, fn, &t, &x0, box)) == 0)
    {
        gmx_fatal(FARGS, "Could not read coordinates from statusfile\n");
    }

    /* Number of shells */
    if (!*nshells)
    {
        gmx_fatal(FARGS, "nshells value must be provided (-sh option)\n");
    }
    fprintf(stderr, "\nDividing the box in %d shells\n", *nshells);

    snew(*shDensity, nr_grps);
    for (i = 0; i < nr_grps; i++)
    {
        snew((*shDensity)[i], *nshells);
    }

    gpbc = gmx_rmpbc_init(&top->idef, ePBC, top->atoms.nr);

    avgRad = 0;

    /*********** Start processing trajectory ***********/
    do
    {
        /* calculate center coordinates */
        calc_recipbox(box, invbox);
        calc_geometric_center(box, invbox, (ePBC == epbcNONE ? 0 : 1), ncenter, index_center, x0, xc);

        /* when pbc, calculate image coordinates nearest to xc */
        if (ePBC != epbcNONE)
        {
            for (i = 0; i < natoms; i++)
            {
                pbc_nearest_image_particle(box, invbox, xc, x0[i], x0[i]);
            }
        }

        /* Determine rmax as the minimum box length*/
        rmax = box[0][0];
        for (ax = 1; ax < DIM; ax++)
        {
            if (box[ax][ax] < rmax)
            {
                rmax = box[ax][ax];
            }
        }
        rmax  *= 0.5;

        if (bRelative)
        {
            *shWidth = 1.0 / (*nshells);
        }
        else
        {
            *shWidth = rmax / (*nshells);
        }
        avgRad  += rmax;

        for (n = 0; n < nr_grps; n++)
        {
            for (i = 0; i < gnx[n]; i++) /* loop over all atoms in index file */
            {
                /* determine shell of atom i */
                rvec_sub(x0[index[n][i]], xc, xd);
                shell = floor(norm(xd) / (*shWidth));
                if (shell < *nshells)
                {
                    shVol = (4*M_PI / 3.0) * (3*shell*(shell + 1) + 1) * pow(*shWidth, 3);
                    sought.nr_el    = 0;
                    sought.atomname = gmx_strdup(*(top->atoms.atomname[index[n][i]]));

                    /* now find the number of electrons. This is not efficient. */
                    found = (t_electron *)
                    bsearch((const void *)&sought,
                            (const void *)eltab, nr, sizeof(t_electron),
                            (int(*)(const void*, const void*))denscom_compare);

                    if (found == nullptr)
                    {
                        fprintf(stderr, "Couldn't find %s. Add it to the .dat file\n",
                                *(top->atoms.atomname[index[n][i]]));
                    }
                    else
                    {
                        (*shDensity)[n][shell] += (found->nr_el -
                                                   top->atoms.atom[index[n][i]].q) / shVol;
                    }
                    free(sought.atomname);
                }
            }
        }
        nr_frames++;
    }
    while (read_next_x(oenv, status, &t, x0, box));
    gmx_rmpbc_done(gpbc);

    /*********** done with status file **********/
    close_trx(status);

    /* shDensity now contains the total number of electrons per shell, summed
       over all frames. Now divide by nr_frames */

    fprintf(stderr, "\nRead %d frames from trajectory. Counting electrons\n",
            nr_frames);

    avgRad  /= nr_frames;
    *shWidth = avgRad / (*nshells);

    for (n = 0; n < nr_grps; n++)
    {
        for (i = 0; i < *nshells; i++)
        {
            (*shDensity)[n][i] /= nr_frames;
        }
    }

    sfree(x0); /* free memory used by coordinate array */
}

static void calc_radial_density(const char *fn, int **index, int gnx[],
                                double ***shDensity, int *nshells, t_topology *top, int ePBC,
                                int nr_grps, real *shWidth, int *index_center, int ncenter,
                                gmx_bool bRelative, const gmx_output_env_t *oenv, const char **dens_opt)
{
    rvec        *x0;            /* coordinates without pbc */
    rvec         xc, xd, xim;
    matrix       box, invbox;   /* box (3x3) */
    double       shVol;
    int          natoms;        /* nr. atoms in trj */
    t_trxstatus *status;
    int        **shCount,       /* nr. of atoms in one shell for a group */
                 i, j, n, ax,   /* loop indices */
                 nr_frames = 0, /* number of frames */
                 shell;         /* current shell */
    char        *buf;    /* for tmp. keeping atomname */
    gmx_rmpbc_t  gpbc = NULL;
    real         t, rmax, avgRad, mindiff;
    real        *den_val; /* values from which the density is calculated */

    /* Read coordinates */
    if ((natoms = read_first_x(oenv, &status, fn, &t, &x0, box)) == 0)
    {
        gmx_fatal(FARGS, "Could not read coordinates from statusfile\n");
    }

    /* Number of shells */
    if (!*nshells)
    {
        gmx_fatal(FARGS, "nshells value must be provided (-sh option)\n");
    }
    fprintf(stderr, "\nDividing the box in %d shells\n", *nshells);

    snew(*shDensity, nr_grps);
    for (i = 0; i < nr_grps; i++)
    {
        snew((*shDensity)[i], *nshells);
    }

    gpbc = gmx_rmpbc_init(&top->idef, ePBC, top->atoms.nr);

    avgRad = 0;

    /*********** Start processing trajectory ***********/

    snew(den_val, top->atoms.nr);
    if (dens_opt[0][0] == 'n')
    {
        for (i = 0; (i < top->atoms.nr); i++)
        {
            den_val[i] = 1;
        }
    }
    else if (dens_opt[0][0] == 'c')
    {
        for (i = 0; (i < top->atoms.nr); i++)
        {
            den_val[i] = top->atoms.atom[i].q;
        }
    }
    else
    {
        for (i = 0; (i < top->atoms.nr); i++)
        {
            den_val[i] = top->atoms.atom[i].m;
        }
    }

    do
    {
        /* calculate center coordinates */
        calc_recipbox(box, invbox);
        calc_geometric_center(box, invbox, (ePBC == epbcNONE ? 0 : 1), ncenter, index_center, x0, xc);

        /* when pbc, calculate image coordinates nearest to xc */
        if (ePBC != epbcNONE)
        {
            for (i = 0; i < natoms; i++)
            {
                pbc_nearest_image_particle(box, invbox, xc, x0[i], x0[i]);
            }
        }

        /* Determine rmax as the minimum box length*/
        rmax = box[0][0];
        for (ax = 1; ax < DIM; ax++)
        {
            if (box[ax][ax] < rmax)
            {
                rmax = box[ax][ax];
            }
        }
        rmax  *= 0.5;
        
        if (bRelative)
        {
            *shWidth = 1.0 / (*nshells);
        }
        else
        {
            *shWidth = rmax / (*nshells);
        }

        avgRad += rmax;

        for (n = 0; n < nr_grps; n++)
        {
            for (i = 0; i < gnx[n]; i++) /* loop over all atoms in index file */
            {
                /* determine shell of atom i */
                rvec_sub(x0[index[n][i]], xc, xd);
                shell = floor(norm(xd) / (*shWidth));
                if (shell < *nshells)
                {
                    shVol = (4*M_PI / 3.0) * (3*shell*(shell + 1) + 1) * pow(*shWidth, 3);
                    (*shDensity)[n][shell] += den_val[index[n][i]] / shVol;
                }
            }
        }
        nr_frames++;
    }
    while (read_next_x(oenv, status, &t, x0, box));
    gmx_rmpbc_done(gpbc);

    /*********** done with status file **********/
    close_trx(status);

    /* shDensity now contains the total mass per shell, summed over all
       frames. Now divide by nr_frames and volume of shell
     */

    fprintf(stderr, "\nRead %d frames from trajectory. Calculating density\n",
            nr_frames);

    avgRad  /= nr_frames;
    *shWidth = avgRad / (*nshells);

    for (n = 0; n < nr_grps; n++)
    {
        for (i = 0; i < *nshells; i++)
        {
            (*shDensity)[n][i] /= nr_frames;
        }
    }

    sfree(x0); /* free memory used by coordinate array */
    sfree(den_val);
}

static void plot_radial_density(double *shDensity[], const char *afile, int nshells,
                                int nr_grps, char *grpname[], real shWidth,
                                const char **dens_opt, gmx_bool bRelative,
                                const gmx_output_env_t *oenv)
{
    FILE       *den;
    const char *title  = nullptr;
    const char *xlabel = nullptr;
    const char *ylabel = nullptr;
    int         shell, n;
    real        rpos, rdens;

    title = "Radial density";

    xlabel = bRelative ?
        "Average relative position from center (nm)" :
        "Relative position from center (nm)";

    switch (dens_opt[0][0])
    {
        case 'm': ylabel = "Density (kg m\\S-3\\N)"; break;
        case 'n': ylabel = "Number density (nm\\S-3\\N)"; break;
        case 'c': ylabel = "Charge density (e nm\\S-3\\N)"; break;
        case 'e': ylabel = "Electron density (e nm\\S-3\\N)"; break;
    }

    den = xvgropen(afile,
                   title, xlabel, ylabel, oenv);

    xvgr_legend(den, nr_grps, (const char**)grpname, oenv);

    for (shell = 0; shell < nshells; shell++)
    {
        rpos = (shell + 0.5) * shWidth;

        fprintf(den, "%12g  ", rpos);
        for (n = 0; (n < nr_grps); n++)
        {
            rdens = shDensity[n][shell];
            if (dens_opt[0][0] == 'm')
            {
                fprintf(den, "   %12g", rdens * AMU / (NANO*NANO*NANO));
            }
            else
            {
                fprintf(den, "   %12g", rdens);
            }
        }
        fprintf(den, "\n");
    }

    xvgrclose(den);
}

int gmx_radial_density(int argc, char *argv[])
{
    const char        *desc[] = {
        "[THISMODULE] computes radial densities for spherical systems, using an index file.[PAR]",
        "IMPORTANT: The centering group must be provided in order to determine the system center",
        "           ",

        "Option [TT]-relative[tt] performs the binning in relative instead of absolute",
        "box coordinates, and scales the final output with the average box dimension",
        "along the output axis.",

        "Densities are in kg/m^3, and number densities or electron densities can also be",
        "calculated. For electron densities, a file describing the number of",
        "electrons for each type of atom should be provided using [TT]-ei[tt].",
        "It should look like::",
        "",
        "   2",
        "   atomname = nrelectrons",
        "   atomname = nrelectrons",
        "",
        "The first line contains the number of lines to read from the file.",
        "There should be one line for each unique atom name in your system.",
        "The number of electrons for each atom is modified by its atomic",
        "partial charge.[PAR]",
        "",
    };

    gmx_output_env_t  *oenv;
    static const char *dens_opt[] =
    { nullptr, "mass", "number", "charge", "electron", nullptr };
    static const char *axtitle     = "R"; /* radial direction           */
    static int         nshells     = 50;  /* nr of shells defined       */
    static int         ngrps       = 1;   /* nr. of groups              */
    static gmx_bool    bRelative   = FALSE;

    t_pargs            pa[]        = {
        { "-sh",  FALSE, etINT, {&nshells},
          "Divide the box in this number of shells." },
        { "-dens",    FALSE, etENUM, {dens_opt},
          "Density"},
        { "-ng",       FALSE, etINT, {&ngrps},
          "Number of groups of which to compute densities." },
        { "-relative", FALSE, etBOOL, {&bRelative},
          "Use relative coordinates for changing boxes and scale output by average dimensions." }
    };

    const char        *bugs[] = {
        "When calculating electron densities, atomnames are used instead of types. This is bad.",
    };

    double           **density;        /* density per shell          */
    real               shWidth;        /* width of one shell         */
    char              *grpname_center; /* centering group name     */
    char             **grpname;        /* groupnames                 */
    int                nr_electrons;   /* nr. electrons              */
    int                ncenter;        /* size of centering group    */
    int               *ngx;            /* sizes of groups            */
    t_electron        *el_tab;         /* tabel with nr. of electrons*/
    t_topology        *top;            /* topology               */
    int                ePBC;
    int               *index_center;   /* index for centering group  */
    int              **index;          /* indices for all groups     */
    int                i;

    t_filenm           fnm[] = { /* files for g_density       */
        { efTRX, "-f", nullptr,  ffREAD },
        { efNDX, nullptr, nullptr,  ffOPTRD },
        { efTPR, nullptr, nullptr,  ffREAD },
        { efDAT, "-ei", "electrons", ffOPTRD }, /* file with nr. of electrons */
        { efXVG, "-o", "density", ffWRITE },
    };

#define NFILE asize(fnm)

    if (!parse_common_args(&argc, argv, PCA_CAN_VIEW | PCA_CAN_TIME,
                           NFILE, fnm, asize(pa), pa, asize(desc), desc, asize(bugs), bugs,
                           &oenv))
    {
        return 0;
    }

    GMX_RELEASE_ASSERT(dens_opt[0] != nullptr, "Option setting inconsistency; dens_opt[0] is NULL");

    top = read_top(ftp2fn(efTPR, NFILE, fnm), &ePBC); /* read topology file */

    snew(grpname, ngrps);
    snew(index, ngrps);
    snew(ngx, ngrps);

    fprintf(stderr,
            "\nNote: that the center of mass is calculated inside the box without applying\n"
            "any special periodicity. If necessary, it is your responsibility to first use\n"
            "trjconv to make sure atoms in this group are placed in the right periodicity.\n\n"
            "Select the group to center density profiles around:\n");
    get_index(&top->atoms, ftp2fn_null(efNDX, NFILE, fnm), 1, &ncenter,
              &index_center, &grpname_center);

    fprintf(stderr, "\nSelect %d group%s to calculate density for:\n", ngrps, (ngrps > 1) ? "s" : "");
    get_index(&top->atoms, ftp2fn_null(efNDX, NFILE, fnm), ngrps, ngx, index, grpname);

    if (dens_opt[0][0] == 'e')
    {
        nr_electrons =  denscom_get_electrons(&el_tab, ftp2fn(efDAT, NFILE, fnm));
        fprintf(stderr, "Read %d atomtypes from datafile\n", nr_electrons);

        calc_radial_electron_density(ftp2fn(efTRX, NFILE, fnm), index, ngx, &density,
                                      &nshells, top, ePBC, ngrps, &shWidth, el_tab,
                                      nr_electrons, index_center, ncenter, bRelative, oenv);
    }
    else
    {
        calc_radial_density(ftp2fn(efTRX, NFILE, fnm), index, ngx, &density, &nshells, top,
                             ePBC, ngrps, &shWidth, index_center, ncenter, bRelative, 
                             oenv, dens_opt);
    }

    plot_radial_density(density, opt2fn("-o", NFILE, fnm), nshells, 
                         ngrps, grpname, shWidth, dens_opt, bRelative, oenv);

    do_view(oenv, opt2fn("-o", NFILE, fnm), "-nxy");  /* view xvgr file */
    return 0;
}
