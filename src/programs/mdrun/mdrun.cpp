/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2004, The GROMACS development team.
 * Copyright (c) 2011,2012,2013,2014,2015,2016,2017, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
/*! \defgroup module_mdrun Implementation of mdrun
 * \ingroup group_mdrun
 *
 * \brief This module contains code that implements mdrun.
 */
/*! \internal \file
 *
 * \brief This file implements mdrun
 *
 * \author Berk Hess <hess@kth.se>
 * \author David van der Spoel <david.vanderspoel@icm.uu.se>
 * \author Erik Lindahl <erik@kth.se>
 * \author Mark Abraham <mark.j.abraham@gmail.com>
 *
 * \ingroup module_mdrun
 */
#include "gmxpre.h"

#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "gromacs/commandline/filenm.h"
#include "gromacs/commandline/pargs.h"
#include "gromacs/domdec/domdec.h"
#include "gromacs/gmxlib/network.h"
#include "gromacs/mdlib/main.h"
#include "gromacs/mdlib/mdrun.h"
#include "gromacs/mdrunutility/handlerestart.h"
#include "gromacs/mdtypes/commrec.h"
#include "gromacs/utility/arraysize.h"
#include "gromacs/utility/fatalerror.h"
#include "gromacs/utility/smalloc.h"
#include "gromacs/mdlib/lpressure.h"

#include "mdrun_main.h"
#include "repl_ex.h"
#include "runner.h"

/*! \brief Return whether either of the command-line parameters that
 *  will trigger a multi-simulation is set */
static bool is_multisim_option_set(int argc, const char *const argv[])
{
    for (int i = 0; i < argc; ++i)
    {
        if (strcmp(argv[i], "-multi") == 0 || strcmp(argv[i], "-multidir") == 0)
        {
            return true;
        }
    }
    return false;
}

//! Implements C-style main function for mdrun
int gmx_mdrun(int argc, char *argv[])
{
    gmx::Mdrunner runner;
    return runner.mainFunction(argc, argv);
}

namespace gmx
{

int Mdrunner::mainFunction(int argc, char *argv[])
{
    const char   *desc[] = {
    "\n"
    "--------------------------------------------------------------------------------\n"
    "[THISMODULE] is a modified version of GROMACS 2018 that allows to calculate\n"
    "the local pressure for planar and spherical systems, using the methods described in\n"
    "\n"
    "R. Goetz, R. Lipowsky, J. Chem. Phys. 108, 7397 (1998).\n"
    "Computer simulations of bilayer membranes: Self-assembly and interfacial tension.\n"
    "\n"    
    "E. Lindahl, O. Edholm, J. Chem. Phys. 113, 3882 (2000).\n"
    "Spatial and energetic-entropic decomposition of surface tension in lipid bilayers\n"
    "from molecular dynamics simulations.\n\n"
    "\n"    
    "T. Nakamura, W. Shinoda, T. Ikeshoji, J. Chem. Phys. 135, 094106 (2011).\n"
    "Novel numerical method for calculating the pressure tensor in spherical\n"
    "coordinates for molecular systems.\n\n"
    "--------------------------------------------------------------------------------\n"
    };

    /* Command line options */
    rvec              realddxyz                                               = {0, 0, 0};
    const char       *ddrank_opt_choices[static_cast<int>(DdRankOrder::nr)+1] =
    { nullptr, "interleave", "pp_pme", "cartesian", nullptr };
    const char       *dddlb_opt_choices[static_cast<int>(DlbOption::nr)+1] =
    { nullptr, "auto", "no", "yes", nullptr };
    const char       *thread_aff_opt_choices[threadaffNR+1] =
    { nullptr, "auto", "on", "off", nullptr };
    const char       *nbpu_opt_choices[] =
    { nullptr, "auto", "cpu", "gpu", nullptr };
    const char       *pme_opt_choices[] =
    { nullptr, "auto", "cpu", "gpu", nullptr };
    const char       *pme_fft_opt_choices[] =
    { nullptr, "auto", "cpu", "gpu", nullptr };
    gmx_bool          bTryToAppendFiles     = TRUE;
    const char       *gpuIdsAvailable       = "";
    const char       *userGpuTaskAssignment = "";

    ImdOptions       &imdOptions = mdrunOptions.imdOptions;

    t_pargs           pa[] = {

        { "-dd",      FALSE, etRVEC, {&realddxyz},
          "Domain decomposition grid, 0 is optimize" },
        { "-ddorder", FALSE, etENUM, {ddrank_opt_choices},
          "DD rank order" },
        { "-npme",    FALSE, etINT, {&domdecOptions.numPmeRanks},
          "Number of separate ranks to be used for PME, -1 is guess" },
        { "-nt",      FALSE, etINT, {&hw_opt.nthreads_tot},
          "Total number of threads to start (0 is guess)" },
        { "-ntmpi",   FALSE, etINT, {&hw_opt.nthreads_tmpi},
          "Number of thread-MPI ranks to start (0 is guess)" },
        { "-ntomp",   FALSE, etINT, {&hw_opt.nthreads_omp},
          "Number of OpenMP threads per MPI rank to start (0 is guess)" },
        { "-ntomp_pme", FALSE, etINT, {&hw_opt.nthreads_omp_pme},
          "Number of OpenMP threads per MPI rank to start (0 is -ntomp)" },
        { "-pin",     FALSE, etENUM, {thread_aff_opt_choices},
          "Whether mdrun should try to set thread affinities" },
        { "-pinoffset", FALSE, etINT, {&hw_opt.core_pinning_offset},
          "The lowest logical core number to which mdrun should pin the first thread" },
        { "-pinstride", FALSE, etINT, {&hw_opt.core_pinning_stride},
          "Pinning distance in logical cores for threads, use 0 to minimize the number of threads per physical core" },
        { "-gpu_id",  FALSE, etSTR, {&gpuIdsAvailable},
          "List of unique GPU device IDs available to use" },
        { "-gputasks",  FALSE, etSTR, {&userGpuTaskAssignment},
          "List of GPU device IDs, mapping each PP task on each node to a device" },
        { "-ddcheck", FALSE, etBOOL, {&domdecOptions.checkBondedInteractions},
          "Check for all bonded interactions with DD" },
        { "-ddbondcomm", FALSE, etBOOL, {&domdecOptions.useBondedCommunication},
          "HIDDENUse special bonded atom communication when [TT]-rdd[tt] > cut-off" },
        { "-rdd",     FALSE, etREAL, {&domdecOptions.minimumCommunicationRange},
          "The maximum distance for bonded interactions with DD (nm), 0 is determine from initial coordinates" },
        { "-rcon",    FALSE, etREAL, {&domdecOptions.constraintCommunicationRange},
          "Maximum distance for P-LINCS (nm), 0 is estimate" },
        { "-dlb",     FALSE, etENUM, {dddlb_opt_choices},
          "Dynamic load balancing (with DD)" },
        { "-dds",     FALSE, etREAL, {&domdecOptions.dlbScaling},
          "Fraction in (0,1) by whose reciprocal the initial DD cell size will be increased in order to "
          "provide a margin in which dynamic load balancing can act while preserving the minimum cell size." },
        { "-ddcsx",   FALSE, etSTR, {&domdecOptions.cellSizeX},
          "HIDDENA string containing a vector of the relative sizes in the x "
          "direction of the corresponding DD cells. Only effective with static "
          "load balancing." },
        { "-ddcsy",   FALSE, etSTR, {&domdecOptions.cellSizeY},
          "HIDDENA string containing a vector of the relative sizes in the y "
          "direction of the corresponding DD cells. Only effective with static "
          "load balancing." },
        { "-ddcsz",   FALSE, etSTR, {&domdecOptions.cellSizeZ},
          "HIDDENA string containing a vector of the relative sizes in the z "
          "direction of the corresponding DD cells. Only effective with static "
          "load balancing." },
        { "-gcom",    FALSE, etINT, {&mdrunOptions.globalCommunicationInterval},
          "Global communication frequency" },
        { "-nb",      FALSE, etENUM, {nbpu_opt_choices},
          "Calculate non-bonded interactions on" },
        { "-nstlist", FALSE, etINT, {&nstlist_cmdline},
          "Set nstlist when using a Verlet buffer tolerance (0 is guess)" },
        { "-tunepme", FALSE, etBOOL, {&mdrunOptions.tunePme},
          "Optimize PME load between PP/PME ranks or GPU/CPU (only with the Verlet cut-off scheme)" },
        { "-pme",     FALSE, etENUM, {pme_opt_choices},
          "Perform PME calculations on" },
        { "-pmefft", FALSE, etENUM, {pme_fft_opt_choices},
          "Perform PME FFT calculations on" },
        { "-v",       FALSE, etBOOL, {&mdrunOptions.verbose},
          "Be loud and noisy" },
        { "-pforce",  FALSE, etREAL, {&pforce},
          "Print all forces larger than this (kJ/mol nm)" },
        { "-reprod",  FALSE, etBOOL, {&mdrunOptions.reproducible},
          "Try to avoid optimizations that affect binary reproducibility" },
        { "-cpt",     FALSE, etREAL, {&mdrunOptions.checkpointOptions.period},
          "Checkpoint interval (minutes)" },
        { "-cpnum",   FALSE, etBOOL, {&mdrunOptions.checkpointOptions.keepAndNumberCheckpointFiles},
          "Keep and number checkpoint files" },
        { "-append",  FALSE, etBOOL, {&bTryToAppendFiles},
          "Append to previous output files when continuing from checkpoint instead of adding the simulation part number to all file names" },
        { "-nsteps",  FALSE, etINT64, {&mdrunOptions.numStepsCommandline},
          "Run this number of steps, overrides .mdp file option (-1 means infinite, -2 means use mdp option, smaller is invalid)" },
        { "-maxh",   FALSE, etREAL, {&mdrunOptions.maximumHoursToRun},
          "Terminate after 0.99 times this time (hours)" },
        { "-multi",   FALSE, etINT, {&nmultisim},
          "Do multiple simulations in parallel" },
        { "-replex",  FALSE, etINT, {&replExParams.exchangeInterval},
          "Attempt replica exchange periodically with this period (steps)" },
        { "-nex",  FALSE, etINT, {&replExParams.numExchanges},
          "Number of random exchanges to carry out each exchange interval (N^3 is one suggestion).  -nex zero or not specified gives neighbor replica exchange." },
        { "-reseed",  FALSE, etINT, {&replExParams.randomSeed},
          "Seed for replica exchange, -1 is generate a seed" },
        { "-imdport",    FALSE, etINT, {&imdOptions.port},
          "HIDDENIMD listening port" },
        { "-imdwait",  FALSE, etBOOL, {&imdOptions.wait},
          "HIDDENPause the simulation while no IMD client is connected" },
        { "-imdterm",  FALSE, etBOOL, {&imdOptions.terminatable},
          "HIDDENAllow termination of the simulation from IMD client" },
        { "-imdpull",  FALSE, etBOOL, {&imdOptions.pull},
          "HIDDENAllow pulling in the simulation from IMD client" },
        { "-rerunvsite", FALSE, etBOOL, {&mdrunOptions.rerunConstructVsites},
          "HIDDENRecalculate virtual site coordinates with [TT]-rerun[tt]" },
        { "-confout", FALSE, etBOOL, {&mdrunOptions.writeConfout},
          "HIDDENWrite the last configuration with [TT]-c[tt] and force checkpointing at the last step" },
        { "-stepout", FALSE, etINT, {&mdrunOptions.verboseStepPrintInterval},
          "HIDDENFrequency of writing the remaining wall clock time for the run" },
        { "-resetstep", FALSE, etINT, {&mdrunOptions.timingOptions.resetStep},
          "HIDDENReset cycle counters after these many time steps" },
        { "-resethway", FALSE, etBOOL, {&mdrunOptions.timingOptions.resetHalfway},
          "HIDDENReset the cycle counters after half the number of steps or halfway [TT]-maxh[tt]" },

        /* local pressure/density options */
        { "-lpres", FALSE, etBOOL, {&mdrunOptions.lpres},
          "Enable local pressure calculation"},
        { "-lpgeo", FALSE, etSTR, {&lpdOptions.geo},
          "Geometry type (planar, spherical; default = planar)"},
        { "-lpnaxis", FALSE, etSTR, {&lpdOptions.nax},
          "Normal axis, only used for planar geometry (X, Y or Z; default = Z)"},
        { "-lpns" , FALSE, etINT, {&lpdOptions.ns},
          "Number of slices/shells where local pressure components are computed (default = 100)"},
        { "-lpmg" , FALSE, etINT, {&lpdOptions.mg},
          "Number of mass groups for density calculation (default = 0)"},
        { "-lpcontrib", FALSE, etSTR, {&lpdOptions.contrib},
          "Contribution type (all, nb, bonds, angles, diheds, pairs, lincs, shake, veloc; default = all)"},
        { "-lpcontour", FALSE, etSTR, {&lpdOptions.contour},
          "Integration contour (planar geometry: IK, H; spherical geometry: only IK; default = IK)"},
        { "-lpfdecomp", FALSE, etSTR, {&lpdOptions.fdecomp},
          "Force decomposition method (REF, GLD; default = REF)"},
    };
    int               rc;
    char            **multidir = nullptr;

    cr = init_commrec();

    unsigned long PCA_Flags = PCA_CAN_SET_DEFFNM;
    // With -multi or -multidir, the file names are going to get processed
    // further (or the working directory changed), so we can't check for their
    // existence during parsing.  It isn't useful to do any completion based on
    // file system contents, either.
    if (is_multisim_option_set(argc, argv))
    {
        PCA_Flags |= PCA_DISABLE_INPUT_FILE_CHECKING;
    }

    /* Comment this in to do fexist calls only on master
     * works not with rerun or tables at the moment
     * also comment out the version of init_forcerec in md.c
     * with NULL instead of opt2fn
     */
    /*
       if (!MASTER(cr))
       {
       PCA_Flags |= PCA_NOT_READ_NODE;
       }
     */

    if (!parse_common_args(&argc, argv, PCA_Flags, nfile, fnm, asize(pa), pa,
                           asize(desc), desc, 0, nullptr, &oenv))
    {
        sfree(cr);
        return 0;
    }

    // Handle the options that permits the user to either declare
    // which compatible GPUs are availble for use, or to select a GPU
    // task assignment. Either could be in an environment variable (so
    // that there is a way to customize it, when using MPI in
    // heterogeneous contexts).
    {
        // TODO Argument parsing can't handle std::string. We should
        // fix that by changing the parsing, once more of the roles of
        // handling, validating and implementing defaults for user
        // command-line options have been seperated.
        hw_opt.gpuIdsAvailable       = gpuIdsAvailable;
        hw_opt.userGpuTaskAssignment = userGpuTaskAssignment;

        const char *env = getenv("GMX_GPU_ID");
        if (env != nullptr)
        {
            if (!hw_opt.gpuIdsAvailable.empty())
            {
                gmx_fatal(FARGS, "GMX_GPU_ID and -gpu_id can not be used at the same time");
            }
            hw_opt.gpuIdsAvailable = env;
        }

        env = getenv("GMX_GPUTASKS");
        if (env != nullptr)
        {
            if (!hw_opt.userGpuTaskAssignment.empty())
            {
                gmx_fatal(FARGS, "GMX_GPUTASKS and -gputasks can not be used at the same time");
            }
            hw_opt.userGpuTaskAssignment = env;
        }

        if (!hw_opt.gpuIdsAvailable.empty() && !hw_opt.userGpuTaskAssignment.empty())
        {
            gmx_fatal(FARGS, "-gpu_id and -gputasks cannot be used at the same time");
        }
    }

    hw_opt.thread_affinity = nenum(thread_aff_opt_choices);

    /* now check the -multi and -multidir option */
    if (opt2bSet("-multidir", nfile, fnm))
    {
        if (nmultisim > 0)
        {
            gmx_fatal(FARGS, "mdrun -multi and -multidir options are mutually exclusive.");
        }
        nmultisim = opt2fns(&multidir, "-multidir", nfile, fnm);
    }


    if (replExParams.exchangeInterval != 0 && nmultisim < 2)
    {
        gmx_fatal(FARGS, "Need at least two replicas for replica exchange (option -multi)");
    }

    if (replExParams.numExchanges < 0)
    {
        gmx_fatal(FARGS, "Replica exchange number of exchanges needs to be positive");
    }

    if (nmultisim >= 1)
    {
#if !GMX_THREAD_MPI
        init_multisystem(cr, nmultisim, multidir, nfile, fnm);
#else
        gmx_fatal(FARGS, "mdrun -multi or -multidir are not supported with the thread-MPI library. "
                  "Please compile GROMACS with a proper external MPI library.");
#endif
    }

    if (!opt2bSet("-cpi", nfile, fnm))
    {
        // If we are not starting from a checkpoint we never allow files to be appended
        // to, since that has caused a ton of strange behaviour and bugs in the past.
        if (opt2parg_bSet("-append", asize(pa), pa))
        {
            // If the user explicitly used the -append option, explain that it is not possible.
            gmx_fatal(FARGS, "GROMACS can only append to files when restarting from a checkpoint.");
        }
        else
        {
            // If the user did not say anything explicit, just disable appending.
            bTryToAppendFiles = FALSE;
        }
    }

    ContinuationOptions &continuationOptions = mdrunOptions.continuationOptions;

    continuationOptions.appendFilesOptionSet = opt2parg_bSet("-append", asize(pa), pa);

    handleRestart(cr, bTryToAppendFiles, nfile, fnm, &continuationOptions.appendFiles, &continuationOptions.startedFromCheckpoint);

    mdrunOptions.rerun            = opt2bSet("-rerun", nfile, fnm);
    mdrunOptions.ntompOptionIsSet = opt2parg_bSet("-ntomp", asize(pa), pa);

    /* Check options for local pressure calculation */
    if (mdrunOptions.lpres)
    {
        if (!opt2bSet("-lpndx", nfile, fnm))
        {
            gmx_fatal(FARGS, "Index file is required for local pressure calculation (use -lpndx index.ndx)");
        }
        else
        {
            printf("\nENABLE LOCAL PRESSURE CALCULATION ...\n\n");
        }
    }
    else
    {
        printf("\nDISABLE LOCAL PRESSURE CALCULATION ...\n\n");
    }   

    /* We postpone opening the log file if we are appending, so we can
       first truncate the old log file and append to the correct position
       there instead.  */
    if (MASTER(cr) && !continuationOptions.appendFiles)
    {
        gmx_log_open(ftp2fn(efLOG, nfile, fnm), cr,
                     continuationOptions.appendFiles, &fplog);
    }
    else
    {
        fplog = nullptr;
    }

    domdecOptions.rankOrder    = static_cast<DdRankOrder>(nenum(ddrank_opt_choices));
    domdecOptions.dlbOption    = static_cast<DlbOption>(nenum(dddlb_opt_choices));
    domdecOptions.numCells[XX] = (int)(realddxyz[XX] + 0.5);
    domdecOptions.numCells[YY] = (int)(realddxyz[YY] + 0.5);
    domdecOptions.numCells[ZZ] = (int)(realddxyz[ZZ] + 0.5);

    nbpu_opt    = nbpu_opt_choices[0];
    pme_opt     = pme_opt_choices[0];
    pme_fft_opt = pme_fft_opt_choices[0];


    rc = mdrunner();

    /* Log file has to be closed in mdrunner if we are appending to it
       (fplog not set here) */
    if (MASTER(cr) && !continuationOptions.appendFiles)
    {
        gmx_log_close(fplog);
    }

    return rc;
}

} // namespace
