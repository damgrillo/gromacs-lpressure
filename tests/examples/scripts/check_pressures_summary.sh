#!/bin/bash

# Set dirs
BINDIR=$(cd $(dirname $0) && pwd)
WORKDIR=$BINDIR/../planar
RESDIR=$WORKDIR/res

# Loop for each system
for SYS in $(ls $RESDIR); do

    # Set system name and water type
    if [ "$SYS" == 'POPE-STDW' ]; then
        SYSNAME=POPE-STDW
    elif [ "$SYS" == 'POPE-PW' ]; then
        SYSNAME=POPE-PW
    elif [ "$SYS" == 'PLC' ]; then
        #SYSNAME=PBD22-PEO14-PRIN27-PRIP43
        SYSNAME=PBD22-PEO14-PRIN54-PRIP86
    elif [ "$SYS" == 'POLY' ]; then
        SYSNAME=PBD22-PEO14
    elif [ "$SYS" == 'STDW' ]; then
        SYSNAME=STDW
    elif [ "$SYS" == 'STDW-IONS' ]; then
        SYSNAME=STDW-IONS
    elif [ "$SYS" == 'PW' ]; then
        SYSNAME=PW
    fi

    # RES file
    RESTXT=$RESDIR/$SYS/$SYSNAME-pressure-summary.txt
    rm $RESTXT  

    XVGNAMES=$(ls $RESDIR/$SYS | grep 'xvgpres-all')
    for XVG in $XVGNAMES; do  

        # Pressure files
        METHOD=${XVG#*-xvgpres-all-}
        METHOD=${METHOD%-SIMD*}
        EDRP_TXT=$(ls $RESDIR/$SYS | grep "edrpres")
        LPALL_TXT=$(ls $RESDIR/$SYS | grep "xvgpres-all" | grep "$METHOD")
        LPVEL_TXT=$(ls $RESDIR/$SYS | grep "xvgpres-veloc")
        EDRP_TXT=$RESDIR/$SYS/$EDRP_TXT
        LPALL_TXT=$RESDIR/$SYS/$LPALL_TXT
        LPVEL_TXT=$RESDIR/$SYS/$LPVEL_TXT

        if [ ! -f $EDRP_TXT ]; then
            echo "ERROR: Unable to find edrpres"
        elif [ ! -f $LPALL_TXT ]; then
            echo "ERROR: Unable to find lpres-all"
        elif [ ! -f $LPVEL_TXT ]; then
            echo "ERROR: Unable to find lpres-veloc"
        else

            echo "Pressure summary for $SYS ..."
        
            # Get total pressure
            PTTOT_EDR=$(cat $EDRP_TXT | grep 'Pt_tot' | awk '{print $3}')
            PNTOT_EDR=$(cat $EDRP_TXT | grep 'Pn_tot' | awk '{print $3}')
            PTTOT_LP=$(cat $LPALL_TXT | grep 'Pt_tot' | awk '{print $3}')
            PNTOT_LP=$(cat $LPALL_TXT | grep 'Pn_tot' | awk '{print $3}')

            # Get kinetic pressure
            PTKIN_EDR=$(cat $EDRP_TXT | grep 'Pt_kin' | awk '{print $3}')
            PNKIN_EDR=$(cat $EDRP_TXT | grep 'Pn_kin' | awk '{print $3}')
            PTKIN_LP=$(cat $LPVEL_TXT | grep 'Pt_tot' | awk '{print $3}')
            PNKIN_LP=$(cat $LPVEL_TXT | grep 'Pn_tot' | awk '{print $3}')

            # Get configurational pressure
            PTCFG_EDR=$(cat $EDRP_TXT | grep 'Pt_cfg' | awk '{print $3}')
            PNCFG_EDR=$(cat $EDRP_TXT | grep 'Pn_cfg' | awk '{print $3}') 
            PTCFG_LP=$(echo "$PTTOT_LP - $PTKIN_LP" | bc -l)
            PNCFG_LP=$(echo "$PNTOT_LP - $PNKIN_LP" | bc -l)

            # Get differences
            PTCFG_DIFF=$(echo "$PTCFG_LP - $PTCFG_EDR" | bc -l)
            PNCFG_DIFF=$(echo "$PNCFG_LP - $PNCFG_EDR" | bc -l)
            PTKIN_DIFF=$(echo "$PTKIN_LP - $PTKIN_EDR" | bc -l)  
            PNKIN_DIFF=$(echo "$PNKIN_LP - $PNKIN_EDR" | bc -l)      
            PTTOT_DIFF=$(echo "$PTTOT_LP - $PTTOT_EDR" | bc -l)  
            PNTOT_DIFF=$(echo "$PNTOT_LP - $PNTOT_EDR" | bc -l) 

            # Write results
            printf "System: $SYS-$METHOD\n\n" >>  $RESTXT
            printf "%12s %12s %12s %12s %12s %12s %12s\n" \
                   " " "Pt_cfg" "Pt_kin" "Pt_tot" "Pn_cfg" "Pn_kin" "Pn_tot" >>  $RESTXT
            printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
                   "LP" "$PTCFG_LP" "$PTKIN_LP" "$PTTOT_LP" "$PNCFG_LP" "$PNKIN_LP" "$PNTOT_LP" >>  $RESTXT
            printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
                   "EDR" "$PTCFG_EDR" "$PTKIN_EDR" "$PTTOT_EDR" "$PNCFG_EDR" "$PNKIN_EDR" "$PNTOT_EDR" >>  $RESTXT
            printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
                   "DIFF" "$PTCFG_DIFF" "$PTKIN_DIFF" "$PTTOT_DIFF" "$PNCFG_DIFF" "$PNKIN_DIFF" "$PNTOT_DIFF" >>  $RESTXT
            printf  "\n\n" >>  $RESTXT

        fi

    done

done


################################################ BACKUP #####################################################

## Loop for each system
#for SYS in $(ls $RESDIR); do

#    # RES file
#    RESTXT=$RESDIR/$SYS/$SYS-pressure-summary.txt
#    rm $RESTXT

#    # EDR file
#    EDRP_TXT=$(ls $RESDIR/$SYS | grep 'edrpres' | head -1)
#    EDRP_TXT=$RESDIR/$SYS/$EDRP_TXT

#    # XVG files
#    for XVG in $(ls $RESDIR/$SYS | grep 'xvgpres'); do
#        XVGP_TXT=$RESDIR/$SYS/$XVG
#        XVGP_SFX=$(echo $XVGP_TXT | sed 's/^.*-xvgpres-//' | sed 's/\.txt$//')
#        XVGP_SYS=$SYS-$XVGP_SFX

#        if [ ! -f $EDRP_TXT ]; then
#            echo "ERROR: Unable to find edrpres"
#        elif [ ! -f $XVGP_TXT ]; then
#            echo "ERROR: Unable to find xvgpres"
#        else

#            echo "Pressure summary for $XVGP_SYS ..."
#        
#            # Get total pressure
#            PTTOT_EDR=$(cat $EDRP_TXT | grep 'Pt_tot' | awk '{print $3}')
#            PNTOT_EDR=$(cat $EDRP_TXT | grep 'Pn_tot' | awk '{print $3}')
#            PTTOT_XVG=$(cat $XVGP_TXT | grep 'Pt_tot' | awk '{print $3}')
#            PNTOT_XVG=$(cat $XVGP_TXT | grep 'Pn_tot' | awk '{print $3}')

#            # Get kinetic pressure
#            PTKIN_EDR=$(cat $EDRP_TXT | grep 'Pt_kin' | awk '{print $3}')
#            PNKIN_EDR=$(cat $EDRP_TXT | grep 'Pn_kin' | awk '{print $3}')
#            PTKIN_XVG=$(cat $XVGP_TXT | grep 'Pt_tot' | awk '{print $3}')
#            PNTOT_XVG=$(cat $XVGP_TXT | grep 'Pn_tot' | awk '{print $3}')

#            # Get differences 
#            PTTOT_DIFF=$(echo "$PTTOT_XVG - $PTTOT_EDR" | bc -l)  
#            PNTOT_DIFF=$(echo "$PNTOT_XVG - $PNTOT_EDR" | bc -l) 

#            # Write results
#            printf "System: $XVGP_SYS\n\n" >>  $RESTXT
#            printf "%12s %12s %12s\n"  " " "Pt_tot" "Pn_tot" >>  $RESTXT
#            printf "%12s %12.4f %12.4f\n" "XVG"  "$PTTOT_XVG"  "$PNTOT_XVG"  >>  $RESTXT
#            printf "%12s %12.4f %12.4f\n" "EDR"  "$PTTOT_EDR"  "$PNTOT_EDR"  >>  $RESTXT
#            printf "%12s %12.4f %12.4f\n" "DIFF" "$PTTOT_DIFF" "$PNTOT_DIFF" >>  $RESTXT
#            printf  "\n\n" >>  $RESTXT

#        fi

#    done

#done
    
