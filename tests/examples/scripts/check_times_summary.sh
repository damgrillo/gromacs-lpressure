#!/bin/bash

# Set dirs
BINDIR=$(cd $(dirname $0) && pwd)
WORKDIR=$BINDIR/../planar
OUTDIR=$WORKDIR/out
RESDIR=$WORKDIR/res

# Loop for each system
for SYS in $(ls $OUTDIR); do

    # RES file
    RESTMP=$RESDIR/$SYS/res.tmp
    RESTXT=$RESDIR/$SYS/$SYS-times-summary.txt
    rm $RESTXT

    # Write header
    printf "#%29s %10s\n" "System" "Time (s)" > $RESTXT

    # LOG files
    LOGLIST=
    for LOG in $(ls $OUTDIR/$SYS | grep 'lpres' | grep '\.log$'); do
        LOG=$OUTDIR/$SYS/$LOG
        SFX=$(echo $LOG | sed 's/^.*-lpres-//' | sed 's/\.log$//')
        LOG_SYS=$SYS-$SFX

        if [ ! -f $LOG ]; then
            echo "ERROR: Unable to find log file"
        else

            echo "Times summary for $LOG_SYS ..."
        
            # Get time
            TIME=$(cat $LOG | grep 'Time:' | tail -1 | awk '{print $3}' )

            # Write results
            printf "%30s %10.3f\n" "$LOG_SYS" "$TIME" >>  $RESTMP
        fi

    done

    # Sort sorted results
    cat $RESTMP | grep 'plainC' | grep 'H-'               >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'H-'  | grep 'REF' >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'H-'  | grep 'GLD' >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'H-'  | grep 'CFD' >>  $RESTXT
    cat $RESTMP | grep 'plainC' | grep 'IK-'              >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'IK-' | grep 'REF' >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'IK-' | grep 'GLD' >>  $RESTXT
    cat $RESTMP | grep 'SIMD'   | grep 'IK-' | grep 'CFD' >>  $RESTXT
    rm $RESTMP    

done


################################### BACKUP #######################################
## Loop for each system
#rm $RESTXT
#for SYS in $(ls $RESDIR); do

#    # Pressure files
#    EDRP_TXT=$(ls $RESDIR/$SYS | grep 'edrpres')
#    LPALL_TXT=$(ls $RESDIR/$SYS | grep 'xvgpres-all-avg')
#    LPVEL_TXT=$(ls $RESDIR/$SYS | grep 'xvgpres-veloc-avg')
#    EDRP_TXT=$RESDIR/$SYS/$EDRP_TXT
#    LPALL_TXT=$RESDIR/$SYS/$LPALL_TXT
#    LPVEL_TXT=$RESDIR/$SYS/$LPVEL_TXT

#    if [ ! -f $EDRP_TXT ]; then
#        echo "ERROR: Unable to find edrpres"
#    elif [ ! -f $LPALL_TXT ]; then
#        echo "ERROR: Unable to find lpres-all-avg"
#    elif [ ! -f $LPVEL_TXT ]; then
#        echo "ERROR: Unable to find lpres-veloc-avg"
#    else

#        echo "Pressure summary for $SYS ..."
#    
#        # Get total pressure
#        PTTOT_EDR=$(cat $EDRP_TXT | grep 'Pt_tot' | awk '{print $3}')
#        PNTOT_EDR=$(cat $EDRP_TXT | grep 'Pn_tot' | awk '{print $3}')
#        PTTOT_LP=$(cat $LPALL_TXT | grep 'Pt_tot' | awk '{print $3}')
#        PNTOT_LP=$(cat $LPALL_TXT | grep 'Pn_tot' | awk '{print $3}')

#        # Get kinetic pressure
#        PTKIN_EDR=$(cat $EDRP_TXT | grep 'Pt_kin' | awk '{print $3}')
#        PNKIN_EDR=$(cat $EDRP_TXT | grep 'Pn_kin' | awk '{print $3}')
#        PTKIN_LP=$(cat $LPVEL_TXT | grep 'Pt_tot' | awk '{print $3}')
#        PNKIN_LP=$(cat $LPVEL_TXT | grep 'Pn_tot' | awk '{print $3}')

#        # Get configurational pressure
#        PTCFG_EDR=$(cat $EDRP_TXT | grep 'Pt_cfg' | awk '{print $3}')
#        PNCFG_EDR=$(cat $EDRP_TXT | grep 'Pn_cfg' | awk '{print $3}') 
#        PTCFG_LP=$(echo "$PTTOT_LP - $PTKIN_LP" | bc -l)
#        PNCFG_LP=$(echo "$PNTOT_LP - $PNKIN_LP" | bc -l)

#        # Get differences
#        PTCFG_DIFF=$(echo "$PTCFG_LP - $PTCFG_EDR" | bc -l)
#        PNCFG_DIFF=$(echo "$PNCFG_LP - $PNCFG_EDR" | bc -l)
#        PTKIN_DIFF=$(echo "$PTKIN_LP - $PTKIN_EDR" | bc -l)  
#        PNKIN_DIFF=$(echo "$PNKIN_LP - $PNKIN_EDR" | bc -l)      
#        PTTOT_DIFF=$(echo "$PTTOT_LP - $PTTOT_EDR" | bc -l)  
#        PNTOT_DIFF=$(echo "$PNTOT_LP - $PNTOT_EDR" | bc -l) 

#        # Write results
#        printf "System: $SYS\n\n" >>  $RESTXT
#        printf "%12s %12s %12s %12s %12s %12s %12s\n" \
#               " " "Pt_cfg" "Pt_kin" "Pt_tot" "Pn_cfg" "Pn_kin" "Pn_tot" >>  $RESTXT
#        printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
#               "LP" "$PTCFG_LP" "$PTKIN_LP" "$PTTOT_LP" "$PNCFG_LP" "$PNKIN_LP" "$PNTOT_LP" >>  $RESTXT
#        printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
#               "EDR" "$PTCFG_EDR" "$PTKIN_EDR" "$PTTOT_EDR" "$PNCFG_EDR" "$PNKIN_EDR" "$PNTOT_EDR" >>  $RESTXT
#        printf "%12s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
#               "DIFF" "$PTCFG_DIFF" "$PTKIN_DIFF" "$PTTOT_DIFF" "$PNCFG_DIFF" "$PNKIN_DIFF" "$PNTOT_DIFF" >>  $RESTXT
#        printf  "\n\n" >>  $RESTXT

#    fi

#done
    
