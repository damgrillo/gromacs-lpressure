#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: check_pressures_edr.sh [options]\n"
    echo -e "OPTIONS:
         -t  Temporary dir
         -i  Input dir
         -o  Output TXT file
         -s  Begin time (optional)
         -e  End time (optional)\n"
}

# Options parser
while getopts "t:i:o:s:e:" OPTION; do
    case $OPTION in
        t)
            TMPDIR=$OPTARG;;
        i)
            INPDIR=$OPTARG;;
        o)
            OUTTXT=$OPTARG;;
        s)
            BTIME=$OPTARG;;
        e)
            ETIME=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $TMPDIR || -z $INPDIR || -z $OUTTXT ]]; then
     usage; exit 1
fi

# Optional flags
OPTFLAGS=''
if [ -n "$BTIME" ]; then OPTFLAGS="$OPTFLAGS -b $BTIME"; fi
if [ -n "$ETIME" ]; then OPTFLAGS="$OPTFLAGS -e $ETIME"; fi

# Set temp files
TMPOPT=$TMPDIR/opt-props.txt
TMPXVG=$TMPDIR/edr-energy.xvg

# Choose properties to measure
echo "Kinetic-En." >  $TMPOPT
echo "Volume"      >> $TMPOPT
echo "Vir-XX"      >> $TMPOPT
echo "Vir-YY"      >> $TMPOPT
echo "Vir-ZZ"      >> $TMPOPT
echo "Pres-XX"     >> $TMPOPT
echo "Pres-YY"     >> $TMPOPT
echo "Pres-ZZ"     >> $TMPOPT

# Write header
printf "#%9s %12s %12s %12s %12s %12s %12s\n" \
       "Job_num" "Pt_tot" "Pn_tot" "Pt_cfg" "Pn_cfg" "Pt_kin" "Pn_kin" >  $OUTTXT

# Loop over EDR files
for EDR in $(ls $INPDIR | grep 'job-[0-9]\+\.edr'); do

    # Get properties
    gmx_LP energy -f $INPDIR/$EDR -o $TMPXVG $OPTFLAGS < $TMPOPT

    # Conversion factor for virial units
    # 1 kJ/mol  = 10e2/Navog bar*nm3 = 16.60539 bar*nm3
    # 1 bar*nm3 = Navog/10e2 kJ/mol  =  0.0602214 kJ/mol
    KJ2B=16.60539 
    B2KJ=0.0602214 

    # Measure average properties 
    # P_ij = (2/V) * (Ek_ij - Vir_ij) 

    JOB_NUM=$(echo $EDR | grep -o 'job-[0-9]\+' | grep -o '[0-9]\+')
    EKN_TOT=$(cat $TMPXVG | grep -v '^[@#]' | awk '{s += $2} END {print s/NR}') 

    PXX_TOT=$(cat $TMPXVG | grep -v '^[@#]' | awk '{s += $7} END {print s/NR}')          
    PYY_TOT=$(cat $TMPXVG | grep -v '^[@#]' | awk '{s += $8} END {print s/NR}')        
    PZZ_TOT=$(cat $TMPXVG | grep -v '^[@#]' | awk '{s += $9} END {print s/NR}')

    PXX_CFG=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s -= (2/$3)*$4*fc} END {print s/NR}')          
    PYY_CFG=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s -= (2/$3)*$5*fc} END {print s/NR}')   
    PZZ_CFG=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s -= (2/$3)*$6*fc} END {print s/NR}')        

    PXX_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s += $7+(2/$3)*$4*fc} END {print s/NR}')          
    PYY_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s += $8+(2/$3)*$5*fc} END {print s/NR}')   
    PZZ_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$KJ2B '{s += $9+(2/$3)*$6*fc} END {print s/NR}')    

    EXX_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$B2KJ '{s += fc*$7*$3/2+$4} END {print s/NR}')          
    EYY_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$B2KJ '{s += fc*$8*$3/2+$5} END {print s/NR}')   
    EZZ_KIN=$(cat $TMPXVG | grep -v '^[@#]' | awk -v fc=$B2KJ '{s += fc*$9*$3/2+$6} END {print s/NR}')      

    PTN_TOT=$(echo "0.5*($PXX_TOT+$PYY_TOT)" | bc -l) 
    PTN_CFG=$(echo "0.5*($PXX_CFG+$PYY_CFG)" | bc -l) 
    PTN_KIN=$(echo "0.5*($PXX_KIN+$PYY_KIN)" | bc -l) 

    EKN_VIR=$(echo "$EXX_KIN+$EYY_KIN+$EZZ_KIN" | bc -l) 

    # Write results
    printf "%10s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
           "$JOB_NUM" "$PTN_TOT" "$PZZ_TOT" "$PTN_CFG" "$PZZ_CFG" "$PTN_KIN" "$PZZ_KIN" >>  $OUTTXT

done

# Write average
PAVG=$(cat $OUTTXT | grep -v '^#' | awk '{for(i=2; i<=NF; i++) s[i]+=$i} END {for(i=2; i<=NF; i++) print s[i]/NR" "}')
PTN_TOT_AVG=$(echo $PAVG | awk '{print $1}')
PZZ_TOT_AVG=$(echo $PAVG | awk '{print $2}')
PTN_CFG_AVG=$(echo $PAVG | awk '{print $3}')
PZZ_CFG_AVG=$(echo $PAVG | awk '{print $4}')
PTN_KIN_AVG=$(echo $PAVG | awk '{print $5}')
PZZ_KIN_AVG=$(echo $PAVG | awk '{print $6}')
printf "%10s %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n" \
       "AVG" "$PTN_TOT_AVG" "$PZZ_TOT_AVG" "$PTN_CFG_AVG" "$PZZ_CFG_AVG" "$PTN_KIN_AVG" "$PZZ_KIN_AVG" >>  $OUTTXT

rm $TMPDIR/*


