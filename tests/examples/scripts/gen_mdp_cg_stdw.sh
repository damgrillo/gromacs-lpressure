#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: gen_mdp.sh [options]\n"
    echo -e "OPTIONS:
         -m  Parameters MDP file
         -l  Timestep length
         -s  Number of timesteps
         -t  Temperature
         -f  Frequency for saving full precision output
         -e  Ensemble: NVT, NPT-iso, NPT-semi, NPT-zfix, SURF
         -g  Surface tension value for SURF mode, in bar*nm
         -p  Lateral pressure value for NPT-semi mode, in bar, optional
         -r  List of residues to be included in GRPSLIST, as string
         -d  List of directories to be included, as string, optional\n"
}

# Options parser
while getopts "m:l:s:f:t:e:g:p:r:d:" OPTION; do
    case $OPTION in
        m)
            MDP=$OPTARG;;
        l)
            TL=$OPTARG;;
        s)
            NS=$OPTARG;;
        f)
            NFQ=$OPTARG;;
        t)
            TEMP=$OPTARG;;
        e)
            ENS=$OPTARG;;
        g)
            GBAR=$OPTARG;;
        p)
            PLAT=$OPTARG;;
        r)
            RESLIST=$OPTARG;;
        d)
            DIRLIST=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $TL || -z $NS || -z $TEMP || -z $MDP ]]; then
     usage; exit 1
fi

# If ensemble mode is not provided, set NPT-iso as default mode
if [[ -z "$ENS" ]]; then
    ENS='NPT-iso'
fi

# If output frequency is not provided, set to 0
if [[ -z "$NFQ" ]]; then
    NFQ=0
fi

# Check ENS options
if [[ "$ENS" != 'NVT'      && "$ENS" != 'NPT-iso' && \
      "$ENS" != 'NPT-semi' && "$ENS" != 'NPT-zfix' && "$ENS" != 'SURF' ]]; then
    echo "ERROR: $ENS is not a valid argument for -e option."
    echo "       Valid arguments are: NVT, NPT-iso, NPT-semi, NPT-zfix, SURF."
    echo '' > $MDP        
    exit 1
fi
if [[ "$ENS" == 'NPT-semi' && -z "$PLAT" ]]; then
    PLAT=1.0
fi
if [[ "$ENS" == 'SURF' ]]; then
    if [[ -z "$GBAR" ]]; then
        echo "ERROR: Surface tension value must be provided in SURF mode (-g option)"
        echo '' > $MDP        
        exit 1
    else
        GBAR=$(echo "$GBAR * 2" | bc -l | awk '{printf "%.3f", $0}')
    fi
fi

# Set frequency for energy calculation
if [ $NFQ -gt 0 ]; then
    NSENER=$NFQ
else
    NSENER='100'
fi

# Set groups list
GRPSLIST="$RESLIST"

# Set temperature coupling parameters
TCMETHOD='nose-hoover'
for GRPS in $GRPSLIST; do
    TAUTLIST="$TAUTLIST 6.0"
    REFTLIST="$REFTLIST $TEMP"
done

# Set pressure coupling parameters
if [ "$ENS" == 'NPT-iso' ]; then
    PCMETHOD='parrinello-rahman'
    PCTYPE='isotropic'
    TAUPLIST='6.0'
    REFPLIST="1.0"
    COMPLIST='4.5e-5 4.5e-5'
elif [ "$ENS" == 'NPT-semi' ]; then
    PCMETHOD='parrinello-rahman'
    PCTYPE='semiisotropic'
    TAUPLIST='6.0'
    REFPLIST="$PLAT 1.0"
    COMPLIST='4.5e-5 4.5e-5'
elif [ "$ENS" == 'NPT-zfix' ]; then
    PCMETHOD='parrinello-rahman'
    PCTYPE='semiisotropic'
    TAUPLIST='6.0'
    REFPLIST="$PLAT 1.0"
    COMPLIST='4.5e-5 0.0'
elif [ "$ENS" == 'SURF' ]; then
    TCMETHOD='v-rescale'
    PCMETHOD='berendsen'
    PCTYPE='surface-tension'
    TAUPLIST='6.0'
    REFPLIST="$GBAR 1.0"
    COMPLIST='4.5e-5 4.5e-5'
fi

# Generate MD simulation parameters file
printf "; PREPROCESSING\n" > $MDP
printf "%-10s = %s %s\n" "define" "-DPOSRES" >> $MDP
if [[ -n "$DIRLIST" ]]; then
    printf "%-10s =" "include" >> $MDP
    for DIR in $DIRLIST; do
        printf " -I$DIR" >> $MDP
    done
    printf "\n" >> $MDP 
fi

printf "\n; RUN CONTROL\n" >> $MDP
printf "%-10s = %-20s\n" "integrator" "md" >> $MDP
printf "%-10s = %-20s\n" "tinit"      "0" >> $MDP
printf "%-10s = %-20s\n" "dt"         "$TL" >> $MDP
printf "%-10s = %-20s\n" "nsteps"     "$NS" >> $MDP
printf "%-10s = %-20s\n" "init-step"  "0" >> $MDP
printf "%-10s = %-20s\n" "comm-mode"  "linear" >> $MDP
printf "%-10s = %-20s\n" "nstcomm"    "100" >> $MDP
printf "%-10s = %-20s\n" "comm-grps"  "System" >> $MDP

printf "\n; LANGEVIN DYNAMICS\n" >> $MDP

printf "\n; ENERGY MINIMIZATION\n" >> $MDP
printf "%-10s = %-20s\n" "emtol"        "10.0" >> $MDP
printf "%-10s = %-20s\n" "emstep"       "0.001" >> $MDP
printf "%-10s = %-20s\n" "nstcgsteep"   "100" >> $MDP
#printf "%-10s = %-20s\n" "nbfgscorr"    "10" >> $MDP

printf "\n; SHELL MOLECULAR DYNAMICS\n" >> $MDP

printf "\n; TEST PARTICLE INSERTION\n" >> $MDP

printf "\n; OUTPUT CONTROL\n" >> $MDP
printf "%-22s = %-20s\n" "nstxout"                  "$NFQ" >> $MDP
printf "%-22s = %-20s\n" "nstvout"                  "$NFQ" >> $MDP
printf "%-22s = %-20s\n" "nstfout"                  "0" >> $MDP
printf "%-22s = %-20s\n" "nstlog"                   "1000" >> $MDP
printf "%-22s = %-20s\n" "nstcalcenergy"            "$NSENER" >> $MDP
printf "%-22s = %-20s\n" "nstenergy"                "$NSENER" >> $MDP
printf "%-22s = %-20s\n" "nstxout-compressed"       "10000" >> $MDP
printf "%-22s = %-20s\n" "compressed-x-precision"   "1000" >> $MDP
printf "%-22s = %-20s\n" "compressed-x-grps"        "System" >> $MDP
printf "%-22s = %-20s\n" "energygrps"               "System" >> $MDP

printf "\n; NEIGHBOUR SEARCHING\n" >> $MDP
printf "%-23s = %-20s\n" "cutoff-scheme"            "verlet" >> $MDP
printf "%-23s = %-20s\n" "nstlist"                  "50" >> $MDP
printf "%-23s = %-20s\n" "nstcalclr"                "1" >> $MDP
printf "%-23s = %-20s\n" "ns-type"                  "grid" >> $MDP
printf "%-23s = %-20s\n" "pbc"                      "xyz" >> $MDP
printf "%-23s = %-20s\n" "periodic-molecules"       "no" >> $MDP
printf "%-23s = %-20s\n" "verlet-buffer-tolerance"  "0.005" >> $MDP
#printf "%-23s = %-20s\n" "rlist"                    "1.2" >> $MDP
#printf "%-23s = %-20s\n" "rlistlong"                "1.5" >> $MDP

## FOR AA RUNS
#printf "\n; ELECTROSTATICS AND VDW\n" >> $MDP
#printf "%-16s = %-20s\n" "coulombtype"      "PME" >> $MDP
#printf "%-16s = %-20s\n" "coulomb-modifier" "Potential-shift-Verlet" >> $MDP
#printf "%-16s = %-20s\n" "rcoulomb-switch"  "0" >> $MDP
#printf "%-16s = %-20s\n" "rcoulomb"         "1.2" >> $MDP
##printf "%-16s = %-20s\n" "epsilon-r"        "1" >> $MDP
##printf "%-16s = %-20s\n" "epsilon-rf"       "0" >> $MDP
#printf "%-16s = %-20s\n" "vdwtype"          "PME" >> $MDP
#printf "%-16s = %-20s\n" "vdw-modifier"     "Potential-shift-Verlet" >> $MDP
##printf "%-16s = %-20s\n" "rvdw-switch"      "0" >> $MDP
#printf "%-16s = %-20s\n" "rvdw"             "1.2" >> $MDP
##printf "%-16s = %-20s\n" "DispCorr"         "Ener" >> $MDP

# FOR CG RUNS
printf "\n; ELECTROSTATICS AND VDW\n" >> $MDP 
printf "%-16s = %-20s\n" "coulombtype"      "Cutoff" >> $MDP # If no charges, use cutoff, else PME
printf "%-16s = %-20s\n" "coulomb-modifier" "Potential-shift-verlet" >> $MDP
printf "%-16s = %-20s\n" "rcoulomb-switch"  "0" >> $MDP
printf "%-16s = %-20s\n" "rcoulomb"         "1.2" >> $MDP
printf "%-16s = %-20s\n" "epsilon-r"        "15" >> $MDP # If polarizable water, use 2.5, else 15
#printf "%-16s = %-20s\n" "epsilon-rf"       "0" >> $MDP
printf "%-16s = %-20s\n" "vdwtype"          "Cutoff" >> $MDP
printf "%-16s = %-20s\n" "vdw-modifier"     "Potential-shift-verlet" >> $MDP
printf "%-16s = %-20s\n" "rvdw-switch"      "0.9" >> $MDP
printf "%-16s = %-20s\n" "rvdw"             "1.2" >> $MDP
#printf "%-16s = %-20s\n" "DispCorr"         "Ener" >> $MDP

#printf "\n; TABLES\n" >> $MDP
#printf "%-15s = %-20s\n" "table-extension" "1" >> $MDP
#printf "%-15s = %-20s\n" "energygrp-table" "" >> $MDP

printf "\n; EWALD\n" >> $MDP
printf "%-16s = %-20s\n" "fourierspacing"   "0.12" >> $MDP
#printf "%-16s = %-20s\n" "fourier-nx"       "0" >> $MDP
#printf "%-16s = %-20s\n" "fourier-ny"       "0" >> $MDP
#printf "%-16s = %-20s\n" "fourier-nz"       "0" >> $MDP
#printf "%-16s = %-20s\n" "pme-order"        "4" >> $MDP
#printf "%-16s = %-20s\n" "ewald-rtol"       "1e-5" >> $MDP
#printf "%-16s = %-20s\n" "ewald-rtol-lj"    "1e-3" >> $MDP
#printf "%-16s = %-20s\n" "lj-pme-comb-rule" "Geometric" >> $MDP
#printf "%-16s = %-20s\n" "ewald-geometry"   "3dc" >> $MDP
#printf "%-16s = %-20s\n" "epsilon-surface"  "0" >> $MDP

if [[ "$ENS" == 'NVT' || "$ENS" =~ 'NPT' || "$ENS" == 'SURF' ]]; then
    printf "\n; TEMPERATURE COUPLING\n" >> $MDP
    printf "%-15s = %-20s\n" "tcoupl"           "$TCMETHOD" >> $MDP
    printf "%-15s = %-20s\n" "nsttcouple"       "10" >> $MDP
    printf "%-15s = %-20s\n" "nh-chain-length"  "1" >> $MDP
    printf "%-15s = %-20s\n" "tc-grps"          "$GRPSLIST" >> $MDP
    printf "%-15s = %-20s\n" "tau-t"            "$TAUTLIST" >> $MDP
    printf "%-15s = %-20s\n" "ref-t"            "$REFTLIST" >> $MDP
fi

if [[ "$ENS" =~ 'NPT' || "$ENS" == 'SURF' ]]; then
    printf "\n; PRESSURE COUPLING\n" >> $MDP
    printf "%-16s = %-20s\n" "pcoupl"           "$PCMETHOD" >> $MDP
    printf "%-16s = %-20s\n" "pcoupltype"       "$PCTYPE" >> $MDP
    printf "%-16s = %-20s\n" "nstpcouple"       "10" >> $MDP
    printf "%-16s = %-20s\n" "tau-p"            "$TAUPLIST" >> $MDP
    printf "%-16s = %-20s\n" "ref-p"            "$REFPLIST" >> $MDP
    printf "%-16s = %-20s\n" "compressibility"  "$COMPLIST" >> $MDP
    printf "%-16s = %-20s\n" "refcoord-scaling" "all" >> $MDP
fi

#printf "\n; SIMULATED ANNEALING\n" >> $MDP
#printf "%-17s = %-20s\n" "annealing"            "$MODELIST" >> $MDP
#printf "%-17s = %-20s\n" "annealing-npoints"    "$POINTLIST" >> $MDP
#printf "%-17s = %-20s\n" "annealing-time"       "$TIMELIST" >> $MDP
#printf "%-17s = %-20s\n" "annealing-temp"       "$TEMPLIST" >> $MDP

#printf "\n; VELOCITY GENERATION\n" >> $MDP
#printf "%-8s = %-20s\n" "gen-vel"   "no" >> $MDP
#printf "%-8s = %-20s\n" "gen-temp"  "293" >> $MDP
#printf "%-8s = %-20s\n" "gen-seed"  "-1" >> $MDP

printf "\n; BONDS\n" >> $MDP
printf "%-20s = %-20s\n" "constraints"          "none" >> $MDP
printf "%-20s = %-20s\n" "constraint-algorithm" "LINCS" >> $MDP
#printf "%-20s = %-20s\n" "continuation"         "no" >> $MDP
#printf "%-20s = %-20s\n" "shake-tol"            "0.0001" >> $MDP
#printf "%-20s = %-20s\n" "lincs-order"          "4" >> $MDP
#printf "%-20s = %-20s\n" "lincs-iter"           "1" >> $MDP
#printf "%-20s = %-20s\n" "lincs-warnangle"      "60" >> $MDP
#printf "%-20s = %-20s\n" "morse"                "no" >> $MDP

#printf "\n; ENERGY GROUP EXCLUSION\n" >> $MDP

#printf "\n; WALLS\n" >> $MDP
#printf "%-15s = %-20s\n" "nwall"            "0" >> $MDP
#printf "%-15s = %-20s\n" "wall-atomtype"    "" >> $MDP
#printf "%-15s = %-20s\n" "wall-type"        "9-3" >> $MDP
#printf "%-15s = %-20s\n" "wall-r-linpot"    "-1" >> $MDP
#printf "%-15s = %-20s\n" "wall-density"     "1" >> $MDP
#printf "%-15s = %-20s\n" "wall-ewald-zfac"  "3" >> $MDP

#printf "\n; COM PULLING\n" >> $MDP
#printf "%-20s = %-20s\n" "pull"                 "constant-force" >> $MDP
#printf "%-20s = %-20s\n" "pull-geometry"        "direction" >> $MDP
#printf "%-20s = %-20s\n" "pull-dim"             "0 0 0" >> $MDP
#printf "%-20s = %-20s\n" "pull-r1"              "1" >> $MDP
#printf "%-20s = %-20s\n" "pull-r0"              "1" >> $MDP
#printf "%-20s = %-20s\n" "pull-constr-tol"      "1e-6" >> $MDP
#printf "%-20s = %-20s\n" "pull-start"           "no" >> $MDP
#printf "%-20s = %-20s\n" "pull-print-reference" "no" >> $MDP
#printf "%-20s = %-20s\n" "pull-nstxout"         "10" >> $MDP
#printf "%-20s = %-20s\n" "pull-nstfout"         "1" >> $MDP
#printf "%-20s = %-20s\n" "pull-ngroups"         "2" >> $MDP
#printf "%-20s = %-20s\n" "pull-group1-name"     "HEAD" >> $MDP
#printf "%-20s = %-20s\n" "pull-group2-name"     "TAIL" >> $MDP
#printf "%-20s = %-20s\n" "pull-group1-weights"  "" >> $MDP
#printf "%-20s = %-20s\n" "pull-group1-pbcatom"  "0" >> $MDP
#printf "%-20s = %-20s\n" "pull-ncoords"         "1" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-groups"   "1 2" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-origin"   "0.0 0.0 0.0" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-vec"      "0.0 0.0 1.0" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-init"     "0.0" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-rate"     "0" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-k"        "$KZ" >> $MDP
#printf "%-20s = %-20s\n" "pull-coord1-kB"       "0" >> $MDP

#printf "\n; NMR REFINEMENT\n" >> $MDP

#printf "\n; FREE ENERGY CALCULATIONS\n" >> $MDP
#printf "%-21s = %-20s\n" "free-energy"              "no" >> $MDP
#printf "%-21s = %-20s\n" "init-lambda"              "-1" >> $MDP
#printf "%-21s = %-20s\n" "delta-lambda"             "0" >> $MDP
#printf "%-21s = %-20s\n" "init-lambda-state"        "-1" >> $MDP
#printf "%-21s = %-20s\n" "fep-lambdas"              "0" >> $MDP
#printf "%-21s = %-20s\n" "coul-lambdas"             "0" >> $MDP
#printf "%-21s = %-20s\n" "vdw-lambdas"              "0" >> $MDP
#printf "%-21s = %-20s\n" "bonded-lambdas"           "0" >> $MDP
#printf "%-21s = %-20s\n" "restraint-lambdas"        "0" >> $MDP
#printf "%-21s = %-20s\n" "mass-lambdas"             "0" >> $MDP
#printf "%-21s = %-20s\n" "temperature-lambdas"      "0" >> $MDP
#printf "%-21s = %-20s\n" "calc-lambda-neighbors"    "" >> $MDP
#printf "%-21s = %-20s\n" "sc-alpha"                 "0" >> $MDP
#printf "%-21s = %-20s\n" "sc-r-power"               "6" >> $MDP
#printf "%-21s = %-20s\n" "sc-coul"                  "no" >> $MDP
#printf "%-21s = %-20s\n" "sc-power"                 "0" >> $MDP
#printf "%-21s = %-20s\n" "sc-sigma"                 "0.3" >> $MDP
#printf "%-21s = %-20s\n" "couple-moltype"           "" >> $MDP
#printf "%-21s = %-20s\n" "couple-lambda0"           "none" >> $MDP
#printf "%-21s = %-20s\n" "couple-lambda1"           "none" >> $MDP
#printf "%-21s = %-20s\n" "couple-intramol"          "no" >> $MDP
#printf "%-21s = %-20s\n" "nstdhdl"                  "100" >> $MDP
#printf "%-21s = %-20s\n" "dhdl-derivatives"         "yes" >> $MDP
#printf "%-21s = %-20s\n" "dhdl-print-energy"        "no" >> $MDP
#printf "%-21s = %-20s\n" "separate-dhdl-file"       "yes" >> $MDP
#printf "%-21s = %-20s\n" "dh-hist-size"             "0" >> $MDP
#printf "%-21s = %-20s\n" "dh-hist-spacing"          "0.1" >> $MDP

#printf "\n; EXPANDED ENSEMBLE CALCULATIONS\n" >> $MDP

#printf "\n; NON-EQUILIBRIUM MD\n" >> $MDP

#printf "\n; ELECTRIC FIELDS\n" >> $MDP

#printf "\n; MIXED QUANTUM/CLASICAL MOLECULAR DYNAMICS\n" >> $MDP

#printf "\n; IMPLICIT SOLVENT\n" >> $MDP

#printf "\n; ADAPTATIVE RESOLUTION SIMULATION\n" >> $MDP

