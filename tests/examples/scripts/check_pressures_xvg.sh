#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: check_pressures_xvg.sh [options]\n"
    echo -e "OPTIONS:
         -t  Temporary dir
         -i  Input dir
         -o  Output TXT file
         -m  Match pattern\n"
}

# Options parser
while getopts "t:i:o:m:" OPTION; do
    case $OPTION in
        t)
            TMPDIR=$OPTARG;;
        i)
            INPDIR=$OPTARG;;
        o)
            OUTTXT=$OPTARG;;
        m)
            MPATT=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $TMPDIR || -z $INPDIR || -z $OUTTXT || -z $MPATT ]]; then
     usage; exit 1
fi

# Write header
printf "#%9s %12s %12s\n" "Job_num" "Ptan" "Pnor" >  $OUTTXT

# Loop over XVG files
for XVG in $(ls $INPDIR | grep "$MPATT\.xvg"); do

    # Measure average pressures
    JOB_NUM=$(echo $XVG | grep -o 'job-[0-9]\+' | grep -o '[0-9]\+')
    PTAN_TOT=$(cat $INPDIR/$XVG | grep -v '^[@#]' | awk '{s += $2} END {print s/NR}')          
    PNOR_TOT=$(cat $INPDIR/$XVG | grep -v '^[@#]' | awk '{s += $3} END {print s/NR}')   

    # Write results
    printf "%10s %12.4f %12.4f\n" "$JOB_NUM" "$PTAN_TOT" "$PNOR_TOT" >>  $OUTTXT

done  

# Write average
PAVG=$(cat $OUTTXT | grep -v '^#' | awk '{pt+=$2; pn+=$3} END {print pt/NR, pn/NR}')
PTAN_AVG=$(echo $PAVG | awk '{print $1}')
PNOR_AVG=$(echo $PAVG | awk '{print $2}')
printf "%10s %12.4f %12.4f\n" "AVG" "$PTAN_AVG" "$PNOR_AVG" >>  $OUTTXT

rm $TMPDIR/*


