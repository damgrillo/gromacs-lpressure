#!/usr/bin/python


import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = pres dir
# sys.argv[2]  = plot file
# sys.argv[3]  = match pattern

# Show usage
if len(sys.argv) < 3:
    sys.exit('Usage: plot_pressure_profiles_planar.py [pres_dir] [plot_file]\n')

# Input variables
presdir   = str(sys.argv[1])
plotfile  = str(sys.argv[2])
matchpatt = str(sys.argv[3])

# Pressure files
pfiles = [os.path.join(presdir, f) for f in os.listdir(presdir) if re.search(matchpatt, f)]

# Get pressure data
pprofs = {}
for ftxt in pfiles:
    fpres = open(ftxt, 'r')
    fread = [line.strip() for line in fpres]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    label = re.search('all-(.*)-new', ftxt).group(1)
    if label not in pprofs:
        pprofs[label] = {'sump':np.array(fdata), 'np':1}
    else:
        pprofs[label]['sump'] += np.array(fdata)
        pprofs[label]['np']   += 1
    fpres.close()

# Get average profiles
avgprofs = {l:p['sump']/p['np'] for l, p in pprofs.items()}

# Line properties
cls = {0:'r', 1:'b', 2:'g', 3:'k', 4:'m', 5:'y', 6:'c'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

# Set plots
nr = 2
lwidth = 2
fig, axes = plt.subplots(nrows=nr, ncols=1, sharex=True, sharey=True, squeeze=True)
if nr == 1:
    axes = [axes]

# Add pressure plots
nc = -1
for lab, pp in sorted(avgprofs.items(), key=itemgetter(0)):
    nc += 1
    z  = pp[:, 0]
    pt = pp[:, 1]
    pn = pp[:, 2]
    axes[0].plot(z, pt, c=cls[nc%7], ls='-', lw=lwidth, label=lab)
    axes[1].plot(z, pn, c=cls[nc%7], ls='-', lw=lwidth, label=lab)

# Set legend and save figure
fs = 14
ll = locs[6]
bp = (0.01, 0.02)

#axes[0].set_xlim(1.0, 18.4)
#axes[0].set_xticks(np.arange(2, 19, 2))
#axes[0].set_ylim(-290, 90)
#axes[0].set_yticks(np.arange(-250, 100, 50))
axes[0].set_ylabel('$P_T(z)$ $\mathsf{(bar)}$', labelpad=18, size=24)
axes[0].legend(loc=ll, fontsize=fs, ncol=2, framealpha=1.0, bbox_to_anchor=bp)
axes[0].margins(0.03, 0.04)
axes[0].grid()

axes[-1].set_xlabel('$z$ $\mathsf{(nm)}$', labelpad=10, size=24)
axes[-1].set_ylabel('$P_N(z)$ $\mathsf{(bar)}$', labelpad=18, size=24)
axes[-1].legend(loc=ll, fontsize=fs, ncol=2, framealpha=1.0, bbox_to_anchor=bp)
axes[-1].margins(0.03, 0.04)
axes[-1].grid()

fig.set_dpi(200)
fig.set_size_inches(10, 14)
fig.tight_layout(h_pad=0.0)
fig.savefig(plotfile)  

