# Axiliar function to show usage
usage() {
    echo -e "usage: gen_top.sh [options]\n"
    echo -e "OPTIONS:
         -n  System Name
         -i  Input System GRO file
         -t  Output System TOP file
         -p  List of ITP files to be included, as string\n"
}

# Options parser
while getopts "n:i:t:p:" OPTION; do
    case $OPTION in
        n)
            SYS=$OPTARG;;
        i)
            GRO=$OPTARG;;
        t)
            TOP=$OPTARG;;
        p)
            ITPLIST=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $SYS || -z $GRO || -z $TOP || -z $ITPLIST ]]; then
     usage; exit 1
fi

# Generate system topology file
prevnum=0
echo -e '\n##################### GENERATE SYSTEM TOP #######################'

# Include ITP files
echo -n "" > $TOP
for ITP in $ITPLIST; do
    echo -e "#include \"$ITP\"" >> $TOP
done

# Write system name and molecules info
echo -e "\n[ system ]" >> $TOP
echo "$SYS" >> $TOP
echo -e "\n[ molecules ]" >> $TOP
printf "%-15s #mols\n" "; Compound" >> $TOP

# Get residue information from GRO file
count=0
reslist=$(cat $GRO | awk '{print $1}' | tail -n +3 | head -n -1 | uniq | sed 's/^[0-9]\+//g')
for res in $reslist; do
    if [[ -z "$prev_res" || "$prev_res" == "$res" ]]; then
        count=$((count+1))
    else  
        printf "%-15s $count\n" "$prev_res" >> $TOP
        printf "%-15s $count\n" "$prev_res"
        count=1
    fi
    prev_res=$res
done
printf "%-15s $count\n" "$res" >> $TOP 
printf "%-15s $count\n" "$res" 
echo '' >> $TOP

