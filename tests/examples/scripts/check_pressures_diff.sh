#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: check_pressures_xvg.sh [options]\n"
    echo -e "OPTIONS:
         -t  Temporary dir
         -i  Input dir
         -o  Output file
         -f  Force decomposition method\n"
}

# Options parser
while getopts "t:i:o:f:" OPTION; do
    case $OPTION in
        t)
            TMPDIR=$OPTARG;;
        i)
            INPDIR=$OPTARG;;
        o)
            RESTXT=$OPTARG;;
        f)
            FDM=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $TMPDIR || -z $INPDIR || -z $RESTXT || -z $FDM ]]; then
     usage; exit 1
fi

# Set files
EDR_TXT=$INPDIR/$(ls $INPDIR | grep 'edrpres')
VEL_TXT=$INPDIR/$(ls $INPDIR | grep 'xvgpres-veloc-new')
ALL_H_TXT=$INPDIR/$(ls $INPDIR | grep "xvgpres-all-H-$FDM")
ALL_IK_TXT=$INPDIR/$(ls $INPDIR | grep "xvgpres-all-IK-$FDM")
CFG_H_TXT=$TMPDIR/cfg-H.txt
CFG_IK_TXT=$TMPDIR/cfg-IK.txt
DIF_VEL_TXT=$TMPDIR/vel-diff.txt
DIF_CFG_H_TXT=$TMPDIR/cfg-H-diff.txt
DIF_CFG_IK_TXT=$TMPDIR/cfg-IK-diff.txt
DIF_ALL_H_TXT=$TMPDIR/all-H-diff.txt
DIF_ALL_IK_TXT=$TMPDIR/all-IK-diff.txt

# Calculate configurational energy
echo '#' > $CFG_H_TXT
echo '#' > $CFG_IK_TXT
paste $ALL_H_TXT $VEL_TXT  | grep -v '^#' | awk '{print $2-$5}'        >> $CFG_H_TXT
paste $ALL_IK_TXT $VEL_TXT | grep -v '^#' | awk '{print $2-$5, $3-$6}' >> $CFG_IK_TXT

# Calculate pressure differences xvgpres-edrpres
paste $EDR_TXT $ALL_H_TXT  | grep -v '^#' | awk '{print $1, $9-$2}'     > $DIF_ALL_H_TXT
paste $EDR_TXT $ALL_IK_TXT | grep -v '^#' | awk '{print $9-$2, $10-$3}' > $DIF_ALL_IK_TXT
paste $EDR_TXT $CFG_H_TXT  | grep -v '^#' | awk '{print $1, $8-$4}'     > $DIF_CFG_H_TXT
paste $EDR_TXT $CFG_IK_TXT | grep -v '^#' | awk '{print $8-$4, $9-$5}'  > $DIF_CFG_IK_TXT
paste $EDR_TXT $VEL_TXT    | grep -v '^#' | awk '{print $9-$6, $10-$7}' > $DIF_VEL_TXT

# Write header
printf "#%9s %12s %12s %12s %12s %12s\n" "Job_num" "Pt_all_H" "Pt_all_IK" "Pn_all_IK" "Pt_kin" "Pn_kin" >  $RESTXT
paste $DIF_ALL_H_TXT $DIF_ALL_IK_TXT $DIF_VEL_TXT | \
      awk '{printf("%10s ", $1); for (i = 2; i <= NF; i++) printf("%12.4f ", $i); printf "\n"}' >> $RESTXT

rm $TMPDIR/*
    
