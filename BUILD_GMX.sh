#!/bin/bash

# Packages location
GMXDIR=$(cd $(dirname $0) && pwd)
INSTALLDIR=$HOME/Software/gromacs-lpressure

# Set build type (Release/Debug)
BTYPE=Release

# Set double precision (ON/OFF)
#DOUBLE=OFF
#BLDIR=build

if [ "$DOUBLE" == 'ON' ]; then
    BLDIR=build-double
else
    BLDIR=build-single
fi

# Set SIMD architechture (AUTO/SSE2/SSE4.1/AVX_256/AVX2_256/AVX_512/AVX_128_FMA/AVX2_128)
#SIMD=AUTO
if [[ -n "$SIMD" && "$SIMD" != 'AUTO' ]]; then
    BLDIR=$BLDIR-$SIMD
fi

# Build GMX
echo
echo "######################################################################################"
mkdir $GMXDIR/$BLDIR
cd $GMXDIR/$BLDIR

#cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLDIR \
#         -DCMAKE_VERBOSE_MAKEFILE=OFF \
#         -DCMAKE_BUILD_TYPE=$BTYPE \
#         -DGMX_BUILD_OWN_FFTW=ON \
#         -DGMX_DOUBLE=$DOUBLE \
#         -DGMX_SIMD=$SIMD \
#         -DGMX_MPI=OFF \
#         -DGMX_GPU=OFF

cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLDIR \
         -DGMX_BUILD_OWN_FFTW=ON \
         -DGMX_DOUBLE=$DOUBLE

make -j4
make install

echo
echo "Compilation finished for $BLDIR ..."
echo "######################################################################################"
echo
echo

